package com.geekhub.task1.task1;

import java.lang.reflect.Field;

public class BeanRepresenter {
    public static <T> void represent(T obj) throws IllegalAccessException {
        Class clazz = obj.getClass();
        System.out.println(clazz);
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (!declaredField.isAccessible()) {
                declaredField.setAccessible(true);
            }
            String name = declaredField.getName();
            Object value = declaredField.get(obj);
            System.out.println(name + " = " + value);
        }
    }
}

