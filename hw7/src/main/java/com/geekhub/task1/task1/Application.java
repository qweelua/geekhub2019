package com.geekhub.task1.task1;

public class Application {
    public static void main(String[] args) throws IllegalAccessException {
        Car car = new Car("black", 250, "sedan", "model s");
        Cat cat = new Cat("white", 4, 4, 20);
        Human human = new Human("human", "man", 18, 70);
        BeanRepresenter.represent(car);
        BeanRepresenter.represent(cat);
        BeanRepresenter.represent(human);
    }
}
