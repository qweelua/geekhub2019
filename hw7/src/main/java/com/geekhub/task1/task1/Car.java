package com.geekhub.task1.task1;

public class Car {
    String color;
    int maxSpeed;
    String type;
    String model;

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }
}
