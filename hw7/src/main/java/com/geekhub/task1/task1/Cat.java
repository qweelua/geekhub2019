package com.geekhub.task1.task1;

public class Cat {
    String color;
    int age;
    int legCount;
    int fullLength;

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }
}
