package com.geekhub.task1.task3;

import java.util.Objects;

public class Car {
    String color;
    int maxSpeed;
    String type;
    String model;

    public Car() {
    }

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return maxSpeed == car.maxSpeed &&
                Objects.equals(color, car.color) &&
                Objects.equals(type, car.type) &&
                Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, maxSpeed, type, model);
    }
}
