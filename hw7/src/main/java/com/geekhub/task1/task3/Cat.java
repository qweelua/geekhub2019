package com.geekhub.task1.task3;

import java.util.Objects;

public class Cat {
    String color;
    int age;
    int legCount;
    int fullLength;

    public Cat() {
    }

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return age == cat.age &&
                legCount == cat.legCount &&
                fullLength == cat.fullLength &&
                Objects.equals(color, cat.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, age, legCount, fullLength);
    }
}
