package com.geekhub.task1.task3;

import java.lang.reflect.Field;

public class BeanComparator {
    public static <T> void beanComparator(T obj1, T obj2) throws IllegalAccessException {
        Field[] firstFields = obj1.getClass().getDeclaredFields();
        Field[] secondFields = obj2.getClass().getDeclaredFields();
        boolean comparator;
        for (int i = 0; i < firstFields.length; i++) {
            comparator = false;
            if (!firstFields[i].isAccessible()) {
                firstFields[i].setAccessible(true);
            }
            if (!secondFields[i].isEnumConstant()) {
                secondFields[i].setAccessible(true);
            }
            String name = firstFields[i].getName();
            Object firstValue = firstFields[i].get(obj1);
            Object secondValue = secondFields[i].get(obj2);
            if (firstValue.equals(secondValue)) {
                comparator = true;
            }
            System.out.println(name + ": " + firstValue + " = " + secondValue + " is " + comparator);

        }
    }
}
