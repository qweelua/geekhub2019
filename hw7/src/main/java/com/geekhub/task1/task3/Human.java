package com.geekhub.task1.task3;

import java.util.Objects;

public class Human {
    String height;
    String gender;
    int age;
    int weight;

    public Human(String height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                weight == human.weight &&
                Objects.equals(height, human.height) &&
                Objects.equals(gender, human.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, gender, age, weight);
    }
}
