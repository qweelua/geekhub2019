package com.geekhub.task1.task3;

public class Application {
    public static void main(String[] args) throws IllegalAccessException {
        Car car1 = new Car("black", 250, "sedan", "model s");
        Car car2 = new Car("black", 290, "sedan", "model x");
        BeanComparator.beanComparator(car1, car2);
    }
}
