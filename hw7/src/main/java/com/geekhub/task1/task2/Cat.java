package com.geekhub.task1.task2;

public class Cat {
    String color;
    int age;
    int legCount;
    int fullLength;

    public Cat() {
    }

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", fullLength=" + fullLength +
                '}';
    }
}
