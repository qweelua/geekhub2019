package com.geekhub.task1.task2;

public class Human {
    String height;
    String gender;
    int age;
    int weight;

    public Human(String height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }
}
