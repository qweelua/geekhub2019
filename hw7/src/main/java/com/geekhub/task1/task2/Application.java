package com.geekhub.task1.task2;

public class Application {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Car car = new Car("black", 250, "sedan", "model s");
        Cat cat = new Cat("white", 4, 4, 20);
        CloneCreator.clone(cat);
        System.out.println(cat.toString());
    }
}
