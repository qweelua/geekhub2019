package com.geekhub.task1.task2;

import java.lang.reflect.Field;

public class CloneCreator {
    public static <T> T clone(T obj) throws IllegalAccessException, InstantiationException {
        T objClone = null;
        objClone = (T) obj.getClass().newInstance();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            field.set(objClone, field.get(obj));
        }
        return objClone;
    }
}
