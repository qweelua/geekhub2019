package com.geekhub.task2.json.adapter;

import com.geekhub.task2.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {

    @Override
    public Object toJson(Collection c) throws JSONException {
        JSONArray jo = new JSONArray();
        for (Object o : c) {
            jo.put(JsonSerializer.serialize(o));
        }
        return jo;
    }
}
