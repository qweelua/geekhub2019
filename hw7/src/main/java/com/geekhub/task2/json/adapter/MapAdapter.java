package com.geekhub.task2.json.adapter;

import com.geekhub.task2.json.JsonSerializer;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */
public class MapAdapter implements JsonDataAdapter<Map> {

    @Override
    public Object toJson(Map map) throws JSONException {
        if (!map.isEmpty()) {
            Set keySet = map.keySet();
            JSONObject jsonObject = new JSONObject();
            for (Object obj : keySet) {
                jsonObject.put(obj.toString(), JsonSerializer.serialize(map.get(obj)));
            }
            return jsonObject;
        }
        return null;
    }
}
