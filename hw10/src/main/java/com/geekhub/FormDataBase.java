package com.geekhub;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class FormDataBase {
    List<FormInformation> formInformations = new CopyOnWriteArrayList<>();

    public FormDataBase(List<FormInformation> formInformations) {
        this.formInformations = formInformations;
    }

    public List<FormInformation> getFormInformations() {
        CopyOnWriteArrayList<FormInformation> sortedList = formInformations.stream()
                .sorted(Comparator
                        .comparing(FormInformation::getLocalTime))
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
        Collections.reverse(sortedList);
        return formInformations;
    }

    public void setFormInformations(List<FormInformation> formInformations) {
        this.formInformations = formInformations;
    }

    public void add(FormInformation formInformation) {
        formInformations.add(formInformation);
    }
}
