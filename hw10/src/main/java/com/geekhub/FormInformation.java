package com.geekhub;

import java.time.LocalDateTime;

public class FormInformation {
    public String name;
    public String message;
    public int rate;
    public LocalDateTime localTime = LocalDateTime.now();

    public FormInformation(String name, String message, int rate) {
        this.name = name;
        this.message = message;
        this.rate = rate;
    }

    public FormInformation() {
    }

    @Override
    public String toString() {
        return "Name= " + name + "\n" +
                ", message= " + message + "\n" +
                ", rate= " + rate + ". ";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public LocalDateTime getLocalTime() {
        return localTime;
    }
}

