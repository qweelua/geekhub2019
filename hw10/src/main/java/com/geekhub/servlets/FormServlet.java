package com.geekhub.servlets;

import com.geekhub.FormDataBase;
import com.geekhub.FormInformation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class FormServlet extends javax.servlet.http.HttpServlet {
    FormDataBase formDataBase = new FormDataBase(new CopyOnWriteArrayList<>());

    String html = "<form method=\"POST\">\n" +
            "    Name: <input name=\"username\" />\n" +
            "    <br><br>\n" +
            "    Message : " +
            "    <br><br>\n" +
            "<textarea name = \"message\"> </textarea> \n" +
            "    <br><br>\n" +
            "    Rate:  <select name = rate>\n" +
            "  <option value=\"1\">1</option>\n" +
            "  <option value=\"2\">2</option>\n" +
            "  <option value=\"3\">3</option>\n" +
            "  <option value=\"4\">4</option>\n" +
            "  <option value=\"5\">5</option>\n" +
            "</select> " +
            "    <br><br>\n" +
            "    <input type=\"submit\" value=\"Submit\" />\n" +
            "</form>";

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        try (PrintWriter writer = response.getWriter()) {
            writer.println(html);
            List<FormInformation> sortedList = formDataBase.getFormInformations();
            if (!formDataBase.getFormInformations().isEmpty()) {
                for (FormInformation formInfo : sortedList) {
                    writer.println(formInfo.toString());
                    writer.println("<br><br>");
                }
            }
        }
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        String name = request.getParameter("username");
        String message = request.getParameter("message");
        String rate = request.getParameter("rate");
        response.sendRedirect("/welcome");
        FormInformation formInformation = new FormInformation(name, message, Integer.parseInt(rate));
        formDataBase.add(formInformation);
    }
}
