create table passengers
(
    id        integer not null
        constraint passengers_pk
            primary key,
    p_name    varchar not null,
    p_surname varchar not null
);

alter table passengers
    owner to postgres;

create table trains
(
    id           integer not null
        constraint trains_pk
            primary key,
    t_name       varchar not null,
    wagons_count integer not null,
    places_count integer not null
);

alter table trains
    owner to postgres;

create table trips
(
    id        integer not null
        constraint trips_pk
            primary key,
    to_city   varchar not null,
    from_city varchar not null,
    train_id  integer not null
        constraint trips_trains_id_fk
            references trains
);

alter table trips
    owner to postgres;

create table tickets
(
    id                integer not null
        constraint tickets_pk
            primary key,
    trip_id           integer not null
        constraint tickets_trips_id_fk
            references trips,
    price             integer,
    passenger_id      integer
        constraint tickets_passengers_id_fk
            references passengers,
    date_of_departure date
);

alter table tickets
    owner to postgres;





insert into trains(id, t_name, wagons_count, places_count) values (1,'Eurostar',5,150)
insert into trains(id, t_name, wagons_count, places_count) values (2,'Thalys',4,200)
insert into trains(id, t_name, wagons_count, places_count) values (3,'AVE S-101',7,280)

insert into trips(id, to_city, from_city, train_id) VALUES (10,'Kiev','Odessa',1)
insert into trips(id, to_city, from_city, train_id) VALUES (11,'Cherkassy','Odessa',2)
insert into trips(id, to_city, from_city, train_id) VALUES (12,'Lviv','Vinnitsya',2)
insert into trips(id, to_city, from_city, train_id) VALUES (13,'Kiev','Zhytomir',3)
insert into trips(id, to_city, from_city, train_id) VALUES (14,'Odessa','Lviv',2)
insert into trips(id, to_city, from_city, train_id) VALUES (15,'Dnepr','Odessa',1)
insert into trips(id, to_city, from_city, train_id) VALUES (16,'Vinnytsya','Cherkassy',3)

insert into passengers(id, p_name, p_surname) VALUES (100,'Volodimir','Zelenskiy')
insert into passengers(id, p_name, p_surname) VALUES (101,'Frank','Ocean')
insert into passengers(id, p_name, p_surname) VALUES (102,'Bohdan','Cherniak')
insert into passengers(id, p_name, p_surname) VALUES (103,'Mark','Tven')
insert into passengers(id, p_name, p_surname) VALUES (104,'Barack','Obama')
insert into passengers(id, p_name, p_surname) VALUES (105,'Donald','Trump')
insert into passengers(id, p_name, p_surname) VALUES (106,'Will','Smith')


insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1000,10,200,100,'2019-12-29')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1001,11,300,100,'2019-12-20')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1002,12,400,100,'2019-12-27')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1003,10,500,102,'2019-12-29')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1004,13,200,103,'2019-12-26')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1005,14,200,103,'2019-12-18')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1006,15,200,103,'2019-12-17')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1007,10,200,104,'2019-12-25')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1008,13,200,104,'2019-12-31')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1009,14,200,105,'2019-12-31')
insert into tickets(id, trip_id, price, passenger_id,date_of_departure) VALUES (1010,12,200,105,'2019-12-30')

