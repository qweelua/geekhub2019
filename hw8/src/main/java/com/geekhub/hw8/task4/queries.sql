-- QUERY TO SELECT TICKETS FOR PASSENGER
select tickets.id, tickets.passenger_id, tickets.price, t.to_city, t.from_city
from tickets
         left join trips t on tickets.trip_id = t.id
where tickets.passenger_id = 100
group by tickets.id, tickets.passenger_id, tickets.price, t.to_city, t.from_city
-- QUERY TO SELECT COUNT OF TICKETS FOR TRIP
select trip_id, count(tickets.trip_id) as count_tickets
from tickets
where trip_id = 10
group by trip_id
-- QUERY TO FIND OUT THE ROUTE OF TRAINS
select trips.train_id, trips.from_city, trips.to_city
from trips
order by train_id
-- QUERY TO GET INFORMATION ABOUT SALES TICKETS AND SUM OF INCOME
select SUM(tickets.price) as sum_of_tickets, count(tickets.id) as count_of_sale_tickets
from tickets
-- QUERY TO GET COUNT OF REGISTER USERS
select count(passengers.id) as count_of_users
from passengers
-- QUERY TO UPDATE AVAILABLE TO FALSE WHERE DATE < NOW
         update tickets
set available = false where date_of_departure<=now()
-- QUERY TO UPDATE PRICE FOR 5% WHERE TRIP_ID
update tickets
set price = price + price * 0.05
where trip_id = 10
-- QUERY TO UPDATE PRICE FOR REGULAR USERS
update tickets
set price = price - price * 0.1
where tickets.passenger_id IN (SELECT passenger_id
                               FROM tickets
                               group by passenger_id
                               having count(passenger_id) > 2)
-- DELETE TICKETS WHERE AVAILABLE FALSE
delete
from tickets
where available = false
--DELETE USERS WHO DIDN'T BUY TICKETS
delete
from passengers
where passengers.id not in (select passenger_id from tickets group by passenger_id)