--TASK1
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       COUNT(states.id) as states_count
FROM countries,
     states
WHERE countries.id = states.country_id
GROUP BY countries.id
ORDER BY states_count desc
LIMIT 1;

--TASK2
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       SUM(cities_count) as cities_of_states
FROM (SELECT states.id, states.country_id, COUNT(cities.id) as cities_count
      FROM states
               LEFT JOIN cities ON states.id = cities.state_id
      GROUP BY states.id
      ORDER BY cities_count DESC) as foo,
     countries
WHERE foo.country_id = countries.id
GROUP BY countries.id
ORDER BY cities_of_states DESC
LIMIT 1;

--TASK3
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       COUNT(states.id) as states_count
FROM countries,
     states
WHERE countries.id = states.country_id
GROUP BY countries.id
ORDER BY states_count DESC, countries.name, countries.id;

--TASK4
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       SUM(cities_count) as cities_of_states
FROM (SELECT states.id, states.country_id, COUNT(cities.id) as cities_count
      FROM states
               LEFT JOIN cities ON states.id = cities.state_id
      GROUP BY states.id
      ORDER BY cities_count DESC) as foo,
     countries
WHERE foo.country_id = countries.id
GROUP BY countries.id
ORDER BY cities_of_states DESC, countries.id, countries.name;

--TASK5
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       COUNT(foo.id)     as states_of_country,
       SUM(cities_count) as cities_of_states
FROM (SELECT states.id, states.country_id, COUNT(cities.id) as cities_count
      FROM states
               LEFT JOIN cities ON states.id = cities.state_id
      GROUP BY states.id
      ORDER BY cities_count DESC) as foo,
     countries
WHERE foo.country_id = countries.id
GROUP BY countries.id
ORDER BY cities_of_states DESC, countries.id, countries.name;

--TASK6
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       foo.name              as state_name,
       MAX(foo.cities_count) as max_cities
FROM countries
         LEFT JOIN (SELECT states.id, states.name, states.country_id, COUNT(cities.id) as cities_count
                    FROM states
                             LEFT JOIN cities ON states.id = cities.state_id
                    GROUP BY states.id, states.name
                    ORDER BY states.id) as foo ON countries.id = foo.country_id
WHERE foo.country_id = countries.id
GROUP BY countries.id, state_name
ORDER BY max_cities DESC
LIMIT 10;

--TASK7
(SELECT countries.id, countries.sortname, countries.name, countries.phonecode, COUNT(states.id) AS states_count
 FROM countries
          LEFT JOIN states ON countries.id = states.country_id
 GROUP BY countries.id, countries.sortname, countries.name, countries.phonecode
 ORDER BY states_count DESC
 LIMIT 10)
UNION ALL
(SELECT countries.id, countries.sortname, countries.name, countries.phonecode, COUNT(states.id) AS states_count
 FROM countries
          LEFT JOIN states ON countries.id = states.country_id
 GROUP BY countries.id, countries.sortname, countries.name, countries.phonecode
 ORDER BY states_count
 LIMIT 10);

--TASK8
SELECT countries.id,
       countries.sortname,
       countries.name,
       countries.phonecode,
       COUNT(states.id) AS count_states
FROM countries
         LEFT JOIN states ON countries.id = states.country_id
GROUP BY countries.id, countries.sortname, countries.name, countries.phonecode
HAVING COUNT(states.id) > (SELECT AVG(count_states)
                           FROM (SELECT countries.id, COUNT(states.id) AS count_states
                                 FROM countries
                                          LEFT JOIN states ON countries.id = states.country_id
                                 GROUP BY countries.id) as foo)
ORDER BY count_states DESC;

--TASK9
SELECT DISTINCT ON (states_count) countries.id,
                                  countries.sortname,
                                  countries.name,
                                  countries.phonecode,
                                  COUNT(states.id) as states_count
FROM countries,
     states
WHERE countries.id = states.country_id
GROUP BY countries.id
ORDER BY states_count, countries.name

--TASK10
SELECT states.name, COUNT(states.name)
FROM states
GROUP BY states.name
HAVING COUNT(states.name) > 1
ORDER BY states.name;

--TASK11
SELECT states.id, states.name, states.country_id, COUNT(cities.id)
FROM states
         LEFT JOIN cities ON states.id = cities.state_id
GROUP BY states.id, states.name, states.country_id
HAVING COUNT(cities.state_id) = 0
ORDER BY states.id;
