package com.geekhub.hw8.task1;

import java.sql.*;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) throws SQLException {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/hw8_task1",
                "postgres",
                "123")) {

            //addNewValueToTableCustomers(connection);
            //addNewValueToTableProduct(connection);
            //addNewValueToTableOrder(connection);
            printTableCustomers(connection);
            printTableOrder(connection);
            printTableProduct(connection);
            //updateTableCustomers(connection, 1);
            //printTableCustomers(connection);
            delateFromTableProduct(connection, 4);
            printTableProduct(connection);
            printListOfCustomersAndTheirTotalSpentMoney(connection);
            printMostPopularProduct(connection);
        }
    }

    private static void delateFromTableProduct(Connection connection, int id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM product WHERE id = " + id)) {
            printCountAndGeneratedId(statement);
        }
    }

    private static void updateTableCustomers(Connection connection, int id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE customers SET first_name = 'Petro'," +
                        " last_name = 'Petrovich', cell_phone = '31203030102' WHERE id =" + id
        )) {
            printCountAndGeneratedId(statement);
        }
    }

    private static void printTableProduct(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM product");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                int currentPrice = resultSet.getInt("current_price");
                System.out.println("id = " + id
                        + ", product_id = " + name + ", quantity = "
                        + description + ", delivery_place = " + currentPrice);
            }
        }
    }

    private static void printTableOrder(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM" + "\"" + "order" + "\"");
            while (resultSet.next()) {
                String deliveryPlace = resultSet.getString("delivery_place");
                int orderId = resultSet.getInt("order_id");
                System.out.println("delivery_place = " + deliveryPlace + ", order_id = " + orderId);
            }
        }
    }

    private static void printTableCustomers(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM customers");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String cellPhone = resultSet.getString("cell_phone");
                System.out.println("id = " + id
                        + ", first_name = " + firstName + ", last_name = "
                        + lastName + ", cell_phone = " + cellPhone);
            }
        }
    }

    private static void addNewValueToTableOrder(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO" + "" + " order" + "s" +
                        "(delivery_place, order_id) VALUES (?,?)",
                Statement.RETURN_GENERATED_KEYS
        )) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter delivery_place of order: ");
            String deliveryPlace = scanner.nextLine();
            System.out.println("Enter order_id: ");
            int orderId = scanner.nextInt();
            statement.setString(1, deliveryPlace);
            statement.setInt(2, orderId);
            printCountAndGeneratedId(statement);
        }
    }

    private static void addNewValueToTableProduct(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO product(id ,name, description, current_price) VALUES (?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS
        )) {
            Scanner in = new Scanner(System.in);
            System.out.println("Put id of product: ");
            int id = in.nextInt();
            System.out.println("Put name of Product: ");
            String name = in.nextLine();
            System.out.println("Put description of product: ");
            String description = in.nextLine();
            System.out.println("Put current price of product: ");
            int currentPrice = in.nextInt();
            statement.setInt(1, id);
            statement.setString(2, name);
            statement.setString(3, description);
            statement.setInt(4, currentPrice);
            printCountAndGeneratedId(statement);
        }
    }

    private static void addNewValueToTableCustomers(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO customers(id,first_name, last_name, cell_phone) VALUES (?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS
        )) {
            Scanner in = new Scanner(System.in);
            System.out.println("Put id of Customer: ");
            int id = in.nextInt();
            System.out.println("Put first name of Customer: ");
            String firstName = in.nextLine();
            System.out.println("Put last name of Customer: ");
            String lastName = in.nextLine();
            System.out.println("Put cell phone of Customer: ");
            String callPhone = in.nextLine();
            statement.setInt(1, id);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, callPhone);
            printCountAndGeneratedId(statement);
        }
    }

    private static void printListOfCustomersAndTheirTotalSpentMoney(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT customers.id,customers.first_name,customers.last_name, " +
                            "sum(orders_with_customers_and_products.quantity*product.current_price) AS sum\n" +
                            "FROM customers,orders_with_customers_and_products,product\n" +
                            "WHERE product.id = orders_with_customers_and_products.product_id AND" +
                            " customers.id = orders_with_customers_and_products.customer_id\n" +
                            "GROUP BY customers.id, customers.first_name, customers.last_name\n" +
                            "ORDER BY id"
            );
            while (resultSet.next()) {
                int customerId = resultSet.getInt("id");
                String customerFirstName = resultSet.getString("first_name");
                String customerLastName = resultSet.getString("last_name");
                int sumTotalSpentMoney = resultSet.getInt("sum");
                System.out.println("Customer id = " + customerId + ", first name = " + customerFirstName +
                        ", last name = " + customerLastName + " , Sum of total spent money = " + sumTotalSpentMoney);
            }
        }
    }

    private static void printMostPopularProduct(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select product.id, product.name, " +
                    "count(orders_with_customers_and_products.product_id) as product_count\n" +
                    "from product,orders_with_customers_and_products\n" +
                    "where product.id = orders_with_customers_and_products.product_id\n" +
                    "group by product.id\n" +
                    "order by product_count desc\n" +
                    "limit 1"
            );
            while (resultSet.next()) {
                int productId = resultSet.getInt("id");
                String productName = resultSet.getString("name");
                int productCount = resultSet.getInt("product_count");
                System.out.println("Product id = " + productId + ", product name = " + productName + ", max count = " + productCount);
            }
        }
    }

    private static void printCountAndGeneratedId(PreparedStatement statement) throws SQLException {
        int count = statement.executeUpdate();
        ResultSet generatedKeys = statement.getGeneratedKeys();
        while (generatedKeys.next()) {
            int generatedId = generatedKeys.getInt(1);
            System.out.println(generatedId);
        }
        System.out.println(count);
    }
}