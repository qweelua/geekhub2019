package com.geekhub.hw8.task2.storage;

import com.geekhub.hw8.task2.objects.Entity;
import com.geekhub.hw8.task2.objects.Ignore;
import com.geekhub.hw8.task2.objects.Name;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example

        String tableName = clazz.getSimpleName().toLowerCase();
        Name annotation = clazz.getAnnotation(Name.class);
        if (annotation != null) {
            tableName = annotation.name();
        }

        String sql = "SELECT * FROM " + tableName + " WHERE id = " + id;

        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String tableName = clazz.getSimpleName().toLowerCase();
        Name annotation = clazz.getAnnotation(Name.class);
        if (annotation != null) {
            tableName = annotation.name();
        }
        String sql = "SELECT * FROM " + tableName;
        List<T> result;
        try (Statement statement = connection.createStatement()) {
            result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        Class<? extends Entity> clazz = entity.getClass();
        List<? extends Entity> list = list(clazz);
        String tableName = clazz.getSimpleName().toLowerCase();
        Name annotation = clazz.getAnnotation(Name.class);
        if (annotation != null) {
            tableName = annotation.name();
        }
        for (Entity obj : list) {
            if (entity.getId().equals(obj.getId())) {
                String sql = "DELETE FROM " + tableName + " WHERE id = " + obj.getId();
                try (Statement statement = connection.createStatement()) {
                    statement.executeUpdate(sql);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> entityMap = prepareEntity(entity);
        if (entity.isNew()) {
            String sql = buildSqlToInsert(entity);
            insertToDb(entity, entityMap, sql);
        } else {
            String sql = buildSqlToUpdate(entity);
            updateInDb(entityMap, sql);
        }
    }

    private void updateInDb(Map<String, Object> entityMap, String sql) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            int counter = 1;
            for (Map.Entry<String, Object> entry : entityMap.entrySet()) {
                Object value = entry.getValue();
                statement.setObject(counter, value);
                counter++;
            }
            statement.executeUpdate();
        }
    }

    private <T extends Entity> void insertToDb(T entity, Map<String, Object> entityMap, String sql) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int counter = 1;
            for (Map.Entry<String, Object> entry : entityMap.entrySet()) {
                Object value = entry.getValue();
                statement.setObject(counter, value);
                counter++;
            }
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            entity.setId(generatedKeys.getInt("id"));
        }
    }

    private <T extends Entity> String buildSqlToInsert(T entity) throws Exception {
        Map<String, Object> entityMap = prepareEntity(entity);
        String tableName = entity.getClass().getSimpleName().toLowerCase();
        Name annotation = entity.getClass().getAnnotation(Name.class);
        StringBuilder params = new StringBuilder();
        StringBuilder values = new StringBuilder();
        if (annotation != null) {
            tableName = annotation.name();
        }
        for (Map.Entry<String, Object> entry : entityMap.entrySet()) {
            String name = entry.getKey();
            if (params.length() > 1) {
                params.append(",");
                values.append(",");
            }
            params.append(name);
            values.append("?");
        }
        return "INSERT INTO " + tableName + "(" + params.toString() + ") VALUES(" + values.toString() + ")";
    }

    private <T extends Entity> String buildSqlToUpdate(T entity) throws Exception {
        Map<String, Object> entityMap = prepareEntity(entity);
        String tableName = entity.getClass().getSimpleName().toLowerCase();
        Name annotation = entity.getClass().getAnnotation(Name.class);
        if (annotation != null) {
            tableName = annotation.name();
        }
        StringBuilder sets = new StringBuilder();
        for (Map.Entry<String, Object> entry : entityMap.entrySet()) {
            String name = entry.getKey();
            String pair = name + " = ?";
            if (sets.length() > 1) {
                sets.append(", ");
            }
            sets.append(pair);
        }
        return "UPDATE " + tableName + " SET " + sets.toString() + " WHERE id = " + entity.getId();
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) continue;
            String fieldName = field.getName();
            Name annotation = field.getAnnotation(Name.class);
            if (annotation != null) {
                fieldName = annotation.name();
            }
            field.setAccessible(true);
            data.put(fieldName, field.get(entity));
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        Field[] declaredFields = clazz.getDeclaredFields();
        Field.setAccessible(declaredFields, true);
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            Constructor<T> constructor = clazz.getConstructor();
            T obj = constructor.newInstance();
            for (int i = 0; i < declaredFields.length; i++) {
                if (declaredFields[i].isAnnotationPresent(Ignore.class)) continue;
                declaredFields[i].set(obj, resultSet.getObject(i + 2));
                obj.setId(resultSet.getInt(1));
            }
            list.add(obj);
        }
        return list;
    }
}
