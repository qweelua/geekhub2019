create table customers
(
    id         serial  not null
        constraint customers_pk
            primary key,
    first_name varchar not null,
    last_name  varchar not null,
    cell_phone varchar
);

alter table customers
    owner to postgres;

create table product
(
    id            serial  not null
        constraint product_pk
            primary key,
    name          varchar not null,
    description   varchar not null,
    current_price integer not null
);

alter table product
    owner to postgres;

create table "order"
(
    delivery_place varchar not null,
    order_id       integer
);

alter table "order"
    owner to postgres;

create table orders_with_customers_and_products
(
    customer_id integer not null
        constraint orders_with_customers_and_products_customers_id_fk
            references customers,
    product_id  integer not null
        constraint orders_with_customers_and_products_product_id_fk
            references product,
    quantity    integer,
    id          integer
);

alter table orders_with_customers_and_products
    owner to postgres;





insert into customers(id, first_name, last_name, cell_phone) VALUES (1,'Mikel','White','+380639306511')
insert into customers(id, first_name, last_name, cell_phone) VALUES (2,'Frank','Ocean','+390109026523')
insert into customers(id, first_name, last_name, cell_phone) VALUES (3,'Barry','Alen','+390693326532')
insert into customers(id, first_name, last_name, cell_phone) VALUES (4,'Spenser','James','+390612306331')

insert into product(id, name, description, current_price) VALUES (10,'Coca-Cola','Water',20)
insert into product(id, name, description, current_price) VALUES (11,'Pepsi','Water',25)
insert into product(id, name, description, current_price) VALUES (12,'Duff','Beer',30)
insert into product(id, name, description, current_price) VALUES (13,'Evian','Mineral water',80)
insert into product(id, name, description, current_price) VALUES (14,'Sprite','Water',15)


insert into "order"(delivery_place, order_id) VALUES ('New-York',101)
insert into "order"(delivery_place, order_id) VALUES ('Los Angeles',102)
insert into "order"(delivery_place, order_id) VALUES ('Chicago',103)


insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (1,10,4,101)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (1,12,5,101)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (1,13,2,101)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (2,13,2,102)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (2,11,2,102)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (2,12,2,102)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (2,14,2,102)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (3,12,20,103)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (3,10,20,103)
insert into orders_with_customers_and_products(customer_id, product_id, quantity, id) VALUES (3,14,20,103)