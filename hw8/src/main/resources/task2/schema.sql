create table cat
(
    id   serial  not null
        constraint cat_pk
            primary key,
    name varchar not null,
    age  integer not null
);

alter table cat
    owner to postgres;

create table users
(
    id      serial           not null
        constraint user_pk
            primary key,
    name    varchar          not null,
    age     integer          not null,
    admin   boolean          not null,
    balance double precision not null,
    date    date
);

alter table users
    owner to postgres;

