create table comment
(
    id            serial
        constraint comment_pk
            primary key,
    username      varchar(20) not null,
    comment       varchar(70) not null,
    rate          int         not null,
    date          varchar(30) not null,
    contractor_id int         not null
);

ALTER TABLE "comment"
    ADD CONSTRAINT "comment_fk0" FOREIGN KEY ("contractor_id") REFERENCES "contractor" ("id");