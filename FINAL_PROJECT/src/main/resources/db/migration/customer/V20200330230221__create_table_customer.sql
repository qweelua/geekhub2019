CREATE TABLE "customer"
(
    "id"       serial NOT NULL,
    "username" char   NOT NULL,
    "password" char   NOT NULL,
    "name"     char   NOT NULL,
    "surname"  char   NOT NULL,
    "age"      char   NOT NULL,
    "gender"   char   NOT NULL,
    "city"     char   NOT NULL,
    CONSTRAINT "customer_pk" PRIMARY KEY ("id")
)