alter table customer
    drop column username;

alter table customer
    drop column password;
