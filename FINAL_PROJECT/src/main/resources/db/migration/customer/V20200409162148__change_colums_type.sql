alter table customer alter column name type varchar(30) using name::varchar(30);
alter table customer alter column surname type varchar(30) using surname::varchar(30);
alter table customer alter column gender type varchar(30) using gender::varchar(30);
alter table customer alter column age type integer using age::integer;
alter table customer alter column city type varchar(30) using city::char;