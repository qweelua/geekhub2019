CREATE TABLE "user"
(
    "id"       serial      NOT NULL,
    "username" varchar(30) NOT NULL,
    "password" varchar(80) NOT NULL,
    "role"     varchar(30) NOT NULL,
    CONSTRAINT "user_pk" PRIMARY KEY ("id")
);