ALTER TABLE "customer" ADD CONSTRAINT "customer_fk0" FOREIGN KEY ("user_id") REFERENCES "user"("id");

ALTER TABLE "contractor" ADD CONSTRAINT "contractor_fk0" FOREIGN KEY ("user_id") REFERENCES "user"("id");