CREATE TABLE "contractor_task_relation"
(
    "contractor_id" int NOT NULL,
    "task_id"       int NOT NULL
);

ALTER TABLE "contractor_task_relation"
    ADD CONSTRAINT "contractor_task_relation_fk0" FOREIGN KEY ("contractor_id") REFERENCES "contractor" ("id");
ALTER TABLE "contractor_task_relation"
    ADD CONSTRAINT "contractor_task_relation_fk1" FOREIGN KEY ("task_id") REFERENCES "task" ("id");