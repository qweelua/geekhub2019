alter table task alter column name type varchar(30) using name::varchar(30);
alter table task alter column "shortDescription" type varchar(80) using name::varchar(80);
alter table task alter column category type varchar(30) using name::varchar(30);