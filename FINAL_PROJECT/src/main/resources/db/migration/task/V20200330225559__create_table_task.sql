CREATE TABLE "task"
(
    "id"               serial NOT NULL,
    "name"             char   NOT NULL,
    "shortDescription" char   NOT NULL,
    "price"            int    NOT NULL,
    "isDone"           bool   NOT NULL,
    "category"         char   NOT NULL,
    CONSTRAINT "task_pk" PRIMARY KEY ("id")
);