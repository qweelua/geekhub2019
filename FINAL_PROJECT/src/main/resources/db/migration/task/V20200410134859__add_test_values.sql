INSERT INTO task(name, "shortDescription", price, "isDone", category, date)
VALUES ('Верстка сайту', 'допомогти зверстати frontend сайту', 150, false, 'IT', '2020-04-14');
INSERT INTO task(name, "shortDescription", price, "isDone", category, date)
VALUES ('Дизайн етикетки', 'розробити дизайн етикетки мінеральної води', 15000, false, 'DESIGN', '2020-04-14');
INSERT INTO task(name, "shortDescription", price, "isDone", category, date)
VALUES ('Прибирання дому', 'зробити генеральне прибирання дому', 400, false, 'DOMESTIC_SERVICES', '2020-04-14');
INSERT INTO task(name, "shortDescription", price, "isDone", category, date)
VALUES ('Зібрати шкаф', 'зібрати шкаф з IKEA по інструкції', 350, false, 'HOME_SERVICES', '2020-04-14');
INSERT INTO task(name, "shortDescription", price, "isDone", category, date)
VALUES ('Забрати з НП', 'забрати й доставити до дому посилку з нової пошти', 200, false,
        'COURIER_SERVICES', '2020-04-14');