INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Petro', 'Petrov', 19, 'MALE','CHERKASSY', 'IT', 2);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Vasyl', 'Vasilich', 23, 'MALE','CHERKASSY', 'DESIGN', 3);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Nikolay', 'Nikolaevich', 40, 'MALE','CHERKASSY', 'DOMESTIC_SERVICES', 4);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Aleksey', 'Alekseyevich', 20, 'MALE','CHERKASSY', 'HOME_SERVICES', 5);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Roman', 'Romanovich', 27, 'MALE','CHERKASSY', 'COURIER_SERVICES', 7);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Georgy', 'Georgievich', 23, 'MALE','CHERKASSY', 'PHOTO_SERVICES', 8);
INSERT INTO contractor(name, surname, age, gender, city, "workCategory", user_id)
VALUES ('Katya', 'Katerinovna', 28, 'MALE','CHERKASSY', 'HOME_SERVICES', 9);
