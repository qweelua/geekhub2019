alter table contractor
    drop column username;

alter table contractor
    drop column password;