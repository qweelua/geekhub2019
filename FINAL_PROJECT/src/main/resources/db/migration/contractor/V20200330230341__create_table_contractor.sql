CREATE TABLE "contractor"
(
    "id"           serial  NOT NULL,
    "username"     char    NOT NULL,
    "password"     char    NOT NULL,
    "name"         char    NOT NULL,
    "surname"      char    NOT NULL,
    "age"          integer NOT NULL,
    "gender"       char    NOT NULL,
    "city"         char    NOT NULL,
    "workCategory" char    NOT NULL,
    CONSTRAINT "contractor_pk" PRIMARY KEY ("id")
)