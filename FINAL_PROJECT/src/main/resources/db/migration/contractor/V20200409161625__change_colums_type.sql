alter table contractor alter column name type varchar(30) using name::varchar(30);
alter table contractor alter column surname type varchar(30) using surname::varchar(30);
alter table contractor alter column gender type varchar(30) using gender::varchar(30);
alter table contractor alter column city type varchar(30) using city::char;
alter table contractor alter column "workCategory" type varchar(30) using city::char;