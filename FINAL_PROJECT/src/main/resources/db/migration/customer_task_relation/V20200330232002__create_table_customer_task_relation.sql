CREATE TABLE "customer_task_relation"
(
    "customer_id" int NOT NULL,
    "task_id"     int NOT NULL
);

ALTER TABLE "customer_task_relation"
    ADD CONSTRAINT "customer_task_relation_fk0" FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");
ALTER TABLE "customer_task_relation"
    ADD CONSTRAINT "customer_task_relation_fk1" FOREIGN KEY ("task_id") REFERENCES "task" ("id");
