package com.geekhub.contractor;

import jdk.vm.ci.meta.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ContractorRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ContractorRowMapper contractorRowMapper;

    @Autowired
    public ContractorRepository(NamedParameterJdbcTemplate jdbcTemplate, ContractorRowMapper contractorRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.contractorRowMapper = contractorRowMapper;
    }

    public Contractor saveContractor(Contractor contractor) {
        String sql = "INSERT INTO contractor" +
                "(name, surname, age, gender, city, \"workCategory\", user_id)" +
                " VALUES (:name,:surname, :age, :gender, :city, :workCategory, :user_id)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id", contractor.getUserId());
        params.addValue("name", contractor.getName());
        params.addValue("surname", contractor.getSurname());
        params.addValue("age", contractor.getAge());
        params.addValue("gender", contractor.getGender().name());
        params.addValue("city", contractor.getCity().name());
        params.addValue("workCategory", contractor.getWorkCategory().name());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder, new String[]{"id"});

        int generatedId = keyHolder.getKey().intValue();

        contractor.setId(generatedId);

        return contractor;
    }

    public List<Contractor> getAllContractors() {
        String sql = "SELECT * FROM contractor ORDER BY id";
        return jdbcTemplate.query(sql, contractorRowMapper);
    }

    public Contractor getContractorById(int id) {
        String sql = "SELECT * FROM contractor WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        return jdbcTemplate.queryForObject(sql, params, contractorRowMapper);
    }

    public Contractor getContractorByUserId(int userId) {
        String sql = "SELECT * FROM contractor WHERE user_id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", userId);

        return jdbcTemplate.queryForObject(sql, params, contractorRowMapper);
    }

    public List<Integer> getContractorsIdWhoFreeByDay(LocalDate date) {
        String sql = "SELECT contractor_id " +
                "FROM contractor_task_relation LEFT JOIN task t2 on contractor_task_relation.task_id = t2.id " +
                "WHERE date NOT like :date";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("date", date.toString());

        return jdbcTemplate.query(sql, params, (rs, rowNum) -> rs.getInt(1));
    }
}
