package com.geekhub.contractor;

import com.geekhub.client.ClientService;
import com.geekhub.client.Gender;
import com.geekhub.contractortask.ContractorTaskRelation;
import com.geekhub.contractortask.ContractorTaskRelationRepository;
import com.geekhub.task.Category;
import com.geekhub.client.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ContractorService extends ClientService {

    private final ContractorRepository contractorRepository;
    private final ContractorTaskRelationRepository contractorTaskRelationRepository;

    @Autowired
    public ContractorService(ContractorRepository contractorRepository,
                             ContractorTaskRelationRepository contractorTaskRelationRepository) {
        this.contractorRepository = contractorRepository;
        this.contractorTaskRelationRepository = contractorTaskRelationRepository;
    }

    public Contractor createContractor(String name, String surname, int age, Gender gender,
                                       City city, Category workCategory, int userId) {
        Contractor contractor = new Contractor();
        contractor.setUserId(userId);
        contractor.setName(name);
        contractor.setSurname(surname);
        contractor.setAge(age);
        contractor.setGender(gender);
        contractor.setCity(city);
        contractor.setWorkCategory(workCategory);

        return contractorRepository.saveContractor(contractor);
    }

    public List<Contractor> getAllContractors() {
        return contractorRepository.getAllContractors();
    }

    public Contractor getContractorById(int contractorId) {
        return contractorRepository.getContractorById(contractorId);
    }

    public Contractor getContractorByUserId(int userId) {
        return contractorRepository.getContractorByUserId(userId);
    }

    public Map<Integer, Integer> getTasksCountByContractorId() {
        return contractorTaskRelationRepository.getTaskCountByContractorId();
    }

    public void addTaskForContractor(Contractor contractor, int taskId) {
        ContractorTaskRelation contractorTaskRelation = new ContractorTaskRelation();
        contractorTaskRelation.setContractorId(contractor.getId());
        contractorTaskRelation.setTaskId(taskId);

        contractorTaskRelationRepository.saveContractorTaskRelation(contractorTaskRelation);
    }

    public float calculateRate(Contractor contractor, int countOfTasks) {
        int countOfDoneTasks = contractorTaskRelationRepository.getDoneTasksByContractorId(contractor.getId());
        if (0 == countOfDoneTasks) {
            return 0;
        }
        return (100 * countOfDoneTasks) / countOfTasks;
    }

    public List<Contractor> getContractorsWhoFreeByDay(String date) {
        List<Integer> contractorIds
                = contractorRepository.getContractorsIdWhoFreeByDay(LocalDate.parse(date));
        List<Contractor> availableContractors = new ArrayList<>();
        for (Integer contractorId : contractorIds) {
            Contractor contractor = contractorRepository.getContractorById(contractorId);
            availableContractors.add(contractor);
        }
        return availableContractors;
    }

}
