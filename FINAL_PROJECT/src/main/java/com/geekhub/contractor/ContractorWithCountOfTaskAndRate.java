package com.geekhub.contractor;

public class ContractorWithCountOfTaskAndRate extends Contractor {
    private int countOfTasks;
    private int rate;

    public int getCountOfTasks() {
        return countOfTasks;
    }

    public void setCountOfTasks(int countOfTasks) {
        this.countOfTasks = countOfTasks;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
