package com.geekhub.contractor;

import com.geekhub.task.Category;
import com.geekhub.client.Client;

public class Contractor extends Client {
    private Category workCategory;

    public Category getWorkCategory() {
        return workCategory;
    }

    public void setWorkCategory(Category workCategory) {
        this.workCategory = workCategory;
    }
}
