package com.geekhub.contractor;

public class ContractorWithCountOfTaskAndRateDto extends ContractorDto {
    private int countOfTasks;
    private float rate;

    public int getCountOfTasks() {
        return countOfTasks;
    }

    public void setCountOfTasks(int countOfTasks) {
        this.countOfTasks = countOfTasks;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
