package com.geekhub.contractor;

import com.geekhub.comment.Comment;
import com.geekhub.comment.CommentDto;
import com.geekhub.comment.CommentDtoConverter;
import com.geekhub.comment.CommentService;
import com.geekhub.contractortask.ContractorTaskRelation;
import com.geekhub.contractortask.ContractorTaskRelationService;
import com.geekhub.task.Task;
import com.geekhub.task.TaskDto;
import com.geekhub.task.TaskDtoConverter;
import com.geekhub.task.TaskService;
import com.geekhub.user.User;
import com.geekhub.user.UserService;
import com.itextpdf.text.DocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class ContractorController {
    private final ContractorService contractorService;
    private final ContractorDtoConverter contractorDtoConverter;
    private final ContractorTaskRelationService contractorTaskRelationService;
    private final CommentService commentService;
    private final CommentDtoConverter commentDtoConverter;
    private final TaskService taskService;
    private final TaskDtoConverter taskDtoConverter;
    private final ContractorToFileConverter contractorToFileConverter;
    private final UserService userService;

    @Autowired
    public ContractorController(ContractorService contractorService, ContractorDtoConverter contractorDtoConverter,
                                ContractorTaskRelationService contractorTaskRelationService, CommentService commentService,
                                CommentDtoConverter commentDtoConverter, TaskService taskService, TaskDtoConverter taskDtoConverter, ContractorToFileConverter contractorToFileConverter, UserService userService) {
        this.contractorService = contractorService;
        this.contractorDtoConverter = contractorDtoConverter;
        this.contractorTaskRelationService = contractorTaskRelationService;
        this.commentService = commentService;
        this.commentDtoConverter = commentDtoConverter;
        this.taskService = taskService;
        this.taskDtoConverter = taskDtoConverter;
        this.contractorToFileConverter = contractorToFileConverter;
        this.userService = userService;
    }

    @GetMapping("/contractor")
    public String getContractors(Model model) {
        List<Contractor> contractors = contractorService.getAllContractors();
        Map<Integer, Integer> tasksCountByContractorId = contractorService.getTasksCountByContractorId();
        List<ContractorWithCountOfTaskAndRateDto> contractorDtos = new ArrayList<>();
        for (Contractor contractor : contractors) {
            Integer countOfTasks = tasksCountByContractorId.get(contractor.getId());
            if (null == countOfTasks) {
                countOfTasks = 0;
            }
            float rate = contractorService.calculateRate(contractor, countOfTasks);
            ContractorWithCountOfTaskAndRateDto contractorDto =
                    contractorDtoConverter.convert(contractor, countOfTasks, rate);
            contractorDtos.add(contractorDto);
        }
        model.addAttribute("contractors", contractorDtos);
        return "contractor/index";
    }

    @GetMapping("/contractor/{contractorId}/show")
    public String getPageAboutContractor(@PathVariable int contractorId, Model model) {
        Contractor contractorById = contractorService.getContractorById(contractorId);
        Map<Integer, Integer> tasksCountByContractorId = contractorService.getTasksCountByContractorId();
        Integer countOfTasks = tasksCountByContractorId.get(contractorId);
        List<ContractorTaskRelation> contractorTaskRelationByContractorId
                = contractorTaskRelationService.getContractorTaskRelationByContractorId(contractorId);
        List<TaskDto> taskDtos = new ArrayList<>();
        for (ContractorTaskRelation taskRelation : contractorTaskRelationByContractorId) {
            int taskId = taskRelation.getTaskId();
            TaskDto taskDto = taskDtoConverter.convert(taskService.getTaskById(taskId));
            taskDtos.add(taskDto);
        }
        if (null == countOfTasks) {
            countOfTasks = 0;
        }
        float rate = contractorService.calculateRate(contractorById, countOfTasks);
        ContractorWithCountOfTaskAndRateDto contractorDto =
                contractorDtoConverter.convert(contractorById, countOfTasks, rate);
        int doneTasks = contractorTaskRelationService.getDoneTasksByContractorId(contractorId);
        model.addAttribute("doneTasks", doneTasks);
        model.addAttribute("contractor", contractorDto);
        model.addAttribute("tasks", taskDtos);
        return "contractor/show";
    }

    @GetMapping("/contractor/{contractorId}/comment")
    public String getPageComment(@PathVariable int contractorId, Model model) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        ContractorDto contractorDto = contractorDtoConverter.convert(contractorService.getContractorById(contractorId));
        List<Comment> comments = commentService.getCommentsByContractorId(contractorId);
        List<CommentDto> commentsDto = new ArrayList<>();
        for (Comment comment : comments) {
            CommentDto commentDto = commentDtoConverter.convert(comment);
            commentsDto.add(commentDto);
        }
        model.addAttribute("username", name);
        model.addAttribute("comments", commentsDto);
        model.addAttribute("avg", commentService.getAvgRateByContractorId(contractorId));
        model.addAttribute("contractorName", contractorDto.getName());
        return "contractor/comment";
    }

    @PostMapping(value = "/contractor/{contractorId}/comment", produces = "application/json")
    public @ResponseBody
    Comment postComment(@PathVariable int contractorId, @RequestBody Comment comment) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        String commentText = comment.getComment();
        int rate = comment.getRate();
        Comment saveComment = commentService.saveComment(name, commentText, rate, contractorId);

        return saveComment;
    }

    @GetMapping("/contractor/my-tasks")
    public String getTasksByContractor(Model model) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUserByUsername(name);
        Contractor contractor = contractorService.getContractorByUserId(user.getId());
        List<TaskDto> taskDtos = new ArrayList<>();
        List<ContractorTaskRelation> contractorTaskRelationByContractorId
                = contractorTaskRelationService.getContractorTaskRelationByContractorId(contractor.getId());
        for (ContractorTaskRelation contractorTaskRelation : contractorTaskRelationByContractorId) {
            Task task = taskService.getTaskById(contractorTaskRelation.getTaskId());
            TaskDto taskDto = taskDtoConverter.convert(task);
            taskDtos.add(taskDto);
        }
        model.addAttribute("tasks", taskDtos);
        return "contractor/my_tasks";
    }

    @GetMapping("/contractor/pdf")
    public void downloadPDFFile(HttpServletResponse response) throws DocumentException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = contractorToFileConverter.createPdfDocument();
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=Contractors.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }

    @GetMapping("/contractor/doc")
    public void downloadDocFile(HttpServletResponse response) throws IOException {
        XWPFDocument document = contractorToFileConverter.createDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Contractors.docx");
        OutputStream os = response.getOutputStream();
        document.write(os);
        os.flush();
        os.close();
    }

    @GetMapping("/contractor/excel")
    public void downloadExcelFile(HttpServletResponse response) throws IOException {
        Workbook excelDocument = contractorToFileConverter.createExcelDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Contractors.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }
}
