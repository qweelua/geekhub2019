package com.geekhub.contractor;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.stereotype.Component;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Component
public class ContractorToFileConverter {
    private final ContractorService contractorService;

    public ContractorToFileConverter(ContractorService contractorService) {
        this.contractorService = contractorService;
    }

    public ByteArrayOutputStream createPdfDocument() throws DocumentException {
        PdfPTable table = new PdfPTable(6);
        table.addCell("Name");
        table.addCell("Surname");
        table.addCell("Age");
        table.addCell("Gender");
        table.addCell("City");
        table.addCell("Work Category");
        List<Contractor> contractors = contractorService.getAllContractors();
        for (Contractor contractor : contractors) {
            table.addCell(contractor.getName());
            table.addCell(contractor.getSurname());
            table.addCell(String.valueOf(contractor.getAge()));
            table.addCell(contractor.getGender().name());
            table.addCell(contractor.getCity().name());
            table.addCell(contractor.getWorkCategory().name());
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(table);
        document.close();
        return byteArrayOutputStream;
    }

    public Workbook createExcelDocument() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Authors");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Name");
        row.createCell(1).setCellValue("Surname");
        row.createCell(2).setCellValue("Age");
        row.createCell(3).setCellValue("Gender");
        row.createCell(4).setCellValue("City");
        row.createCell(5).setCellValue("Work Category");
        int rowIter = 1;
        List<Contractor> contractors = contractorService.getAllContractors();
        for (Contractor contractor : contractors) {
            Row tempRaw = sheet.createRow(rowIter);
            rowIter++;
            tempRaw.createCell(0).setCellValue(contractor.getName());
            tempRaw.createCell(1).setCellValue(contractor.getSurname());
            tempRaw.createCell(2).setCellValue(contractor.getAge());
            tempRaw.createCell(3).setCellValue(contractor.getGender().name());
            tempRaw.createCell(4).setCellValue(contractor.getCity().getName());
            tempRaw.createCell(5).setCellValue(contractor.getWorkCategory().getName());


        }
        return workbook;
    }

    public XWPFDocument createDocument() {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable();
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("Name");
        tableRowOne.addNewTableCell().setText("Surname");
        tableRowOne.addNewTableCell().setText("Age");
        tableRowOne.addNewTableCell().setText("Gender");
        tableRowOne.addNewTableCell().setText("City");
        tableRowOne.addNewTableCell().setText("workcategory");
        List<Contractor> contractors = contractorService.getAllContractors();
        for (Contractor contractor : contractors) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(contractor.getName());
            tableRowTwo.getCell(1).setText(contractor.getSurname());
            tableRowTwo.getCell(2).setText(String.valueOf(contractor.getAge()));
            tableRowTwo.getCell(3).setText(contractor.getGender().name());
            tableRowTwo.getCell(4).setText(contractor.getCity().getName());
            tableRowTwo.getCell(5).setText(contractor.getWorkCategory().getName());
        }
        return document;
    }
}

