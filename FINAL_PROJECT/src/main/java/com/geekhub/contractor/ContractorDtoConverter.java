package com.geekhub.contractor;

import com.geekhub.client.GenderDto;
import com.geekhub.task.CategoryDto;
import com.geekhub.client.CityDto;
import org.springframework.stereotype.Component;

@Component
public class ContractorDtoConverter {

    public ContractorDto convert(Contractor contractor) {
        ContractorDto contractorDto = new ContractorDto();
        contractorDto.setId(contractor.getId());
        contractorDto.setName(contractor.getName());
        contractorDto.setSurname(contractor.getSurname());
        contractorDto.setAge(contractor.getAge());
        contractorDto.setGender(GenderDto.valueOf(contractor.getGender().name()));
        contractorDto.setCity(CityDto.valueOf(contractor.getCity().name()));
        contractorDto.setWorkCategory(CategoryDto.valueOf(contractor.getWorkCategory().name()));
        contractorDto.setUserId(contractor.getUserId());

        return contractorDto;
    }

    public ContractorWithCountOfTaskAndRateDto convert(Contractor contractor, int countOfTasks, float rate) {
        ContractorWithCountOfTaskAndRateDto contractorDto = new ContractorWithCountOfTaskAndRateDto();
        contractorDto.setId(contractor.getId());
        contractorDto.setName(contractor.getName());
        contractorDto.setSurname(contractor.getSurname());
        contractorDto.setAge(contractor.getAge());
        contractorDto.setGender(GenderDto.valueOf(contractor.getGender().name()));
        contractorDto.setCity(CityDto.valueOf(contractor.getCity().name()));
        contractorDto.setWorkCategory(CategoryDto.valueOf(contractor.getWorkCategory().name()));
        contractorDto.setUserId(contractor.getUserId());
        contractorDto.setCountOfTasks(countOfTasks);
        contractorDto.setRate(rate);

        return contractorDto;
    }
}
