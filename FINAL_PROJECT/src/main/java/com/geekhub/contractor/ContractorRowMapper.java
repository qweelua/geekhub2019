package com.geekhub.contractor;

import com.geekhub.task.Category;
import com.geekhub.client.Gender;
import com.geekhub.client.City;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ContractorRowMapper implements RowMapper<Contractor> {

    @Override
    public Contractor mapRow(ResultSet rs, int rowNum) throws SQLException {
        Contractor contractor = new Contractor();
        contractor.setId(rs.getInt("id"));
        contractor.setUserId(rs.getInt("user_id"));
        contractor.setName(rs.getString("name"));
        contractor.setSurname(rs.getString("surname"));
        contractor.setAge(rs.getInt("age"));
        contractor.setGender(Gender.valueOf(rs.getString("gender")));
        contractor.setCity(City.valueOf(rs.getString("city")));
        contractor.setWorkCategory(Category.valueOf(rs.getString("workCategory")));

        return contractor;
    }
}
