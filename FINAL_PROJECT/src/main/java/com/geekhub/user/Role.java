package com.geekhub.user;

public enum Role {
    ROLE_CONTRACTOR("Виконавець"), ROLE_CUSTOMER("Замовник");

    String roleName;

    public String getRoleName() {
        return roleName;
    }

    private Role(String roleName) {
        this.roleName = roleName;
    }
}
