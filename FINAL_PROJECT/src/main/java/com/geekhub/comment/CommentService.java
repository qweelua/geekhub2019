package com.geekhub.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Comment saveComment(String username, String commentText, int rate, int contractorId) {
        Comment comment = new Comment();
        comment.setUsername(username);
        comment.setComment(commentText);
        comment.setRate(rate);
        comment.setContractorId(contractorId);
        comment.setDate(LocalDate.now());
        return commentRepository.saveComment(comment);
    }

    public List<Comment> getCommentsByContractorId(int contractorId) {
        return commentRepository.getCommentsByContractorId(contractorId);
    }

    public float getAvgRateByContractorId(int contractorId) {
        return commentRepository.getAvgRateByContractorId(contractorId);
    }
}
