package com.geekhub.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public class CommentRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public CommentRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Comment saveComment(Comment comment) {
        String sql = "INSERT INTO comment(username, comment, rate, date, contractor_id) " +
                "VALUES(:username, :comment, :rate, :date, :contractor_id)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", comment.getUsername());
        params.addValue("comment", comment.getComment());
        params.addValue("rate", comment.getRate());
        params.addValue("date", comment.getDate().toString());
        params.addValue("contractor_id", comment.getContractorId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder, new String[]{"id"});

        int id = keyHolder.getKey().intValue();
        comment.setId(id);

        return comment;
    }

    public List<Comment> getCommentsByContractorId(int contractorId) {
        String sql = "SELECT * FROM comment WHERE contractor_id = :contractorId ORDER BY id DESC ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractorId", contractorId);

        return jdbcTemplate.query(sql, params, (rs, rowNum) -> {
            Comment comment = new Comment();
            comment.setId(rs.getInt("id"));
            comment.setUsername(rs.getString("username"));
            comment.setComment(rs.getString("comment"));
            comment.setRate(rs.getInt("rate"));
            comment.setDate(LocalDate.parse(rs.getString("date")));
            comment.setContractorId(rs.getInt("contractor_id"));
            return comment;
        });
    }

    public float getAvgRateByContractorId(int contractorId) {
        String sql = "SELECT AVG(rate) FROM comment WHERE contractor_id=:contractorId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractorId", contractorId);
        Float rate = jdbcTemplate.queryForObject(sql, params, Float.TYPE);
        if (null == rate) {
            return 0;
        } else return rate;
    }
}

