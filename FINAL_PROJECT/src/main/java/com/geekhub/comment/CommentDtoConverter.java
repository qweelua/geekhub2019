package com.geekhub.comment;

import org.springframework.stereotype.Component;

@Component
public class CommentDtoConverter {

    public CommentDto convert(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setId(comment.getId());
        commentDto.setUsername(comment.getUsername());
        commentDto.setComment(comment.getComment());
        commentDto.setRate(comment.getRate());
        commentDto.setDate(comment.getDate());
        commentDto.setContractorId(comment.getContractorId());

        return commentDto;
    }
}
