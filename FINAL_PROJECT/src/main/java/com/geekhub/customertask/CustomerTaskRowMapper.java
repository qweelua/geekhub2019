package com.geekhub.customertask;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CustomerTaskRowMapper implements RowMapper<CustomerTaskRelation> {

    @Override
    public CustomerTaskRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerTaskRelation customerTaskRelation = new CustomerTaskRelation();
        customerTaskRelation.setCustomerId(rs.getInt("customer_id"));
        customerTaskRelation.setTaskId(rs.getInt("task_id"));
        return customerTaskRelation;
    }
}
