package com.geekhub.customertask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CustomerTaskRelationRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CustomerTaskRowMapper customerTaskRowMapper;

    @Autowired
    public CustomerTaskRelationRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                          CustomerTaskRowMapper customerTaskRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.customerTaskRowMapper = customerTaskRowMapper;
    }

    public void saveCustomerTaskRelation(CustomerTaskRelation customerTaskRelation) {
        String sql = "INSERT INTO customer_task_relation(customer_id, task_id) VALUES(:customerId, :taskId)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("customerId", customerTaskRelation.getCustomerId());
        params.addValue("taskId", customerTaskRelation.getTaskId());

        jdbcTemplate.update(sql, params);
    }

    public Map<Integer, Integer> getTaskCountByCustomerId() {
        String sql = "SELECT customer_id, count(task_id) as" +
                " count_tasks FROM customer_task_relation GROUP BY customer_id";
        Map<Integer, Integer> countOfTasksByCustomerId = jdbcTemplate.query(sql,
                (ResultSetExtractor<Map<Integer, Integer>>) rs -> {
                    Map<Integer, Integer> tasksCountByCustomerId = new HashMap<>();
                    while (rs.next()) {
                        Integer customerId = rs.getInt("customer_id");
                        Integer countTasks = rs.getInt("count_tasks");
                        tasksCountByCustomerId.put(customerId, countTasks);
                    }
                    return tasksCountByCustomerId;
                });
        return countOfTasksByCustomerId;
    }

    public List<CustomerTaskRelation> getCustomerTaskRelationByCustomerId(int customerId) {
        String sql = "SELECT customer_id, task_id FROM customer_task_relation WHERE customer_id = " + customerId;
        return jdbcTemplate.query(sql, customerTaskRowMapper);
    }

    public List<CustomerTaskRelation> getCustomerTaskRelationByTaskId(int taskId) {
        String sql = "SELECT customer_id, task_id FROM customer_task_relation WHERE task_id = :taskId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("taskId", taskId);

        return jdbcTemplate.query(sql, params, customerTaskRowMapper);
    }
}
