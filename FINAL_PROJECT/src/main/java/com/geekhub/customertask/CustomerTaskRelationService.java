package com.geekhub.customertask;

import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class CustomerTaskRelationService {

    private final CustomerTaskRelationRepository customerTaskRelationRepository;

    public CustomerTaskRelationService(CustomerTaskRelationRepository customerTaskRelationRepository) {
        this.customerTaskRelationRepository = customerTaskRelationRepository;
    }

    public List<CustomerTaskRelation> getCustomerTaskRelationByCustomerId(int customerId) {
        return customerTaskRelationRepository.getCustomerTaskRelationByCustomerId(customerId);
    }

    public List<CustomerTaskRelation> getCustomerTaskRelationByTaskId(int taskId) {
        return customerTaskRelationRepository.getCustomerTaskRelationByTaskId(taskId);
    }

    public Map<Integer, Integer> getTasksCountByCustomerId() {
        return customerTaskRelationRepository.getTaskCountByCustomerId();
    }
}
