package com.geekhub.client;

public enum CityDto {
    VINITSYA("Вінниця"), DNEPR("Дніпро"), ZHITOMIR("Житомир"), UZHOROD("Ужгород"),
    ZAPORIZHIE("Запоріжжя"), KIEV("Київ"), LYCK("Луцьк"), KROPIVNITSKIY("Кропивницький"),
    LUGANSK("Луганск"), DONECK("Донецьк"), LVIV("Львів"), NIKOLAEV("Миколаїв"),
    ODESA("Одеса"), POLTAVA("Полтава"), CHERKASSY("Черкаси");

    private String name;

    private CityDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
