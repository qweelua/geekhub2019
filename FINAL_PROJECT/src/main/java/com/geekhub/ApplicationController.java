package com.geekhub;

import com.geekhub.contractor.Contractor;
import com.geekhub.contractor.ContractorDtoConverter;
import com.geekhub.contractor.ContractorService;
import com.geekhub.contractor.ContractorWithCountOfTaskAndRateDto;
import com.geekhub.task.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class ApplicationController {
    private final ContractorService contractorService;
    private final ContractorDtoConverter contractorDtoConverter;
    private final TaskService taskService;
    private final TaskDtoConverter taskDtoConverter;

    public ApplicationController(ContractorService contractorService, ContractorDtoConverter contractorDtoConverter, TaskService taskService, TaskDtoConverter taskDtoConverter) {
        this.contractorService = contractorService;
        this.contractorDtoConverter = contractorDtoConverter;
        this.taskService = taskService;
        this.taskDtoConverter = taskDtoConverter;
    }

    @GetMapping("/")
    public String homePage(Model model) {
        List<Contractor> contractors = contractorService.getAllContractors();
        Map<Integer, Integer> tasksCountByContractorId = contractorService.getTasksCountByContractorId();
        List<ContractorWithCountOfTaskAndRateDto> contractorsDto = new ArrayList<>();
        for (Contractor contractor : contractors) {
            Integer countOfTasks = tasksCountByContractorId.get(contractor.getId());
            if (null == countOfTasks) {
                countOfTasks = 0;
            }
            float rate = contractorService.calculateRate(contractor, countOfTasks);
            ContractorWithCountOfTaskAndRateDto contractorDto
                    = contractorDtoConverter.convert(contractor, countOfTasks, rate);
            contractorsDto.add(contractorDto);
        }
        Comparator<ContractorWithCountOfTaskAndRateDto> comparatorContractors = Comparator
                .comparing(ContractorWithCountOfTaskAndRateDto::getRate);
        List<ContractorWithCountOfTaskAndRateDto> topContractorsByRate = contractorsDto
                .stream()
                .sorted(comparatorContractors.reversed())
                .limit(4)
                .collect(Collectors.toList());
        List<Task> tasks = taskService.getAllTask();
        List<TaskDto> taskDtos = new ArrayList<>();
        for (Task task : tasks) {
            TaskDto taskDto = taskDtoConverter.convert(task);
            taskDtos.add(taskDto);
        }
        Comparator<TaskDto> comparatorTask = Comparator.comparing(TaskDto::getPrice);
        List<TaskDto> taskByPriceAndIsNotDone = taskDtos
                .stream()
                .filter(task -> task.getIsDone() == false)
                .sorted(comparatorTask.reversed())
                .limit(4)
                .collect(Collectors.toList());
        model.addAttribute("contractors", topContractorsByRate);
        model.addAttribute("tasks", taskByPriceAndIsNotDone);

        return "home/main";
    }
}
