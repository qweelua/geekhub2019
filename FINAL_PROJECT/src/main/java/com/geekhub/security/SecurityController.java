package com.geekhub.security;

import com.geekhub.client.City;
import com.geekhub.client.Gender;
import com.geekhub.contractor.ContractorService;
import com.geekhub.customer.CustomerService;
import com.geekhub.task.Category;
import com.geekhub.user.Role;
import com.geekhub.user.User;
import com.geekhub.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.time.LocalDate;


@Controller
public class SecurityController {
    private final UserService userService;
    private final CustomerService customerService;
    private final ContractorService contractorService;

    @Autowired
    public SecurityController(UserService userService, CustomerService customerService, ContractorService contractorService) {
        this.userService = userService;
        this.customerService = customerService;
        this.contractorService = contractorService;
    }

    @GetMapping("/reg")
    public String registration(Model model) {
        model.addAttribute("roles", Role.values());
        return "security/reg";
    }


    @PostMapping("/reg")
    public String saveUser(@RequestParam String username, @RequestParam String password, @RequestParam String role,
                           RedirectAttributes redirectAttributes) {
        userService.saveUser(username, password, role);
        redirectAttributes.addFlashAttribute("username", username);
        if (role.equals("ROLE_CUSTOMER")) {
            return "redirect:/reg/customer";
        } else return "redirect:/reg/contractor";
    }

    @GetMapping("/reg/customer")
    public String customerRegistration(Model model, @ModelAttribute("username") String username) {
        model.addAttribute("genders", Gender.values());
        model.addAttribute("citys", City.values());
        model.addAttribute("username", username);

        return "security/customer";
    }

    @PostMapping("/reg/customer")
    public String saveCustomer(@RequestParam String name, @RequestParam String surname,
                               @RequestParam String birthDate, @RequestParam String gender,
                               @RequestParam String city, @RequestParam String username) {

        User user = userService.getUserByUsername(username);
        int id = user.getId();
        int age = customerService.calculateAge(LocalDate.parse(birthDate));

        customerService.createCustomer(name, surname, age, Gender.valueOf(gender), City.valueOf(city), id);

        return "redirect:/";
    }

    @GetMapping("/reg/contractor")
    public String contractorRegistration(Model model, @ModelAttribute("username") String username) {
        model.addAttribute("genders", Gender.values());
        model.addAttribute("citys", City.values());
        model.addAttribute("categories", Category.values());
        model.addAttribute("username", username);

        return "security/contractor";
    }

    @PostMapping("/reg/contractor")
    public String saveContractor(@RequestParam String name, @RequestParam String surname,
                                 @RequestParam String birthDate, @RequestParam String gender,
                                 @RequestParam String city, @RequestParam String category, @RequestParam String username) {
        User user = userService.getUserByUsername(username);
        int id = user.getId();
        int age = contractorService.calculateAge(LocalDate.parse(birthDate));
        contractorService.createContractor(name, surname, age, Gender.valueOf(gender), City.valueOf(city),
                Category.valueOf(category), id);

        return "redirect:/";
    }


    @GetMapping("login")
    public String login() {
        return "security/login";
    }

    @PostMapping("login")
    public String loginRedirect() {
        return "redirect:/";
    }
}
