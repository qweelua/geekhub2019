package com.geekhub.contractortask;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractorTaskRelationService {

    private final ContractorTaskRelationRepository contractorTaskRelationRepository;

    public ContractorTaskRelationService(ContractorTaskRelationRepository contractorTaskRelationRepository) {
        this.contractorTaskRelationRepository = contractorTaskRelationRepository;
    }

    public List<ContractorTaskRelation> getContractorTaskRelationByContractorId(int contractorId) {
        return contractorTaskRelationRepository.getContractorTaskRelationByContractorId(contractorId);
    }

    public List<ContractorTaskRelation> getContractorTaskRelationByTaskId(int taskId) {
        return contractorTaskRelationRepository.getContractorTaskRelationByTaskId(taskId);
    }

    public int getDoneTasksByContractorId(int contractorId) {
        return contractorTaskRelationRepository.getDoneTasksByContractorId(contractorId);
    }
}
