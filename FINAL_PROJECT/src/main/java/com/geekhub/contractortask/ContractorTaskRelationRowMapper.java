package com.geekhub.contractortask;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ContractorTaskRelationRowMapper implements RowMapper<ContractorTaskRelation> {

    @Override
    public ContractorTaskRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
        ContractorTaskRelation contractorTaskRelation = new ContractorTaskRelation();
        contractorTaskRelation.setContractorId(rs.getInt("contractor_id"));
        contractorTaskRelation.setTaskId(rs.getInt("task_id"));

        return contractorTaskRelation;
    }
}
