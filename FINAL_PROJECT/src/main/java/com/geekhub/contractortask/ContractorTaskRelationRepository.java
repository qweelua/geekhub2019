package com.geekhub.contractortask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContractorTaskRelationRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ContractorTaskRelationRowMapper contractorTaskRelationRowMapper;

    @Autowired
    public ContractorTaskRelationRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                            ContractorTaskRelationRowMapper contractorTaskRelationRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.contractorTaskRelationRowMapper = contractorTaskRelationRowMapper;
    }

    public void saveContractorTaskRelation(ContractorTaskRelation contractorTaskRelation) {
        String sql = "INSERT INTO contractor_task_relation(contractor_id, task_id) VALUES(:contractorId, :taskId)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractorId", contractorTaskRelation.getContractorId());
        params.addValue("taskId", contractorTaskRelation.getTaskId());

        jdbcTemplate.update(sql, params);
    }

    public Map<Integer, Integer> getTaskCountByContractorId() {
        String sql = "SELECT contractor_id, count(task_id) as count_tasks" +
                " FROM contractor_task_relation GROUP BY contractor_id";
        Map<Integer, Integer> countOfTasksByContractorId = jdbcTemplate.query(sql,
                (ResultSetExtractor<Map<Integer, Integer>>) rs -> {
                    Map<Integer, Integer> tasksCountByContractorId = new HashMap<>();
                    while (rs.next()) {
                        Integer contractorId = rs.getInt("contractor_id");
                        Integer countTasks = rs.getInt("count_tasks");
                        tasksCountByContractorId.put(contractorId, countTasks);
                    }
                    return tasksCountByContractorId;
                });
        return countOfTasksByContractorId;
    }

    public List<ContractorTaskRelation> getContractorTaskRelationByContractorId(int contractorId) {
        String sql = "SELECT contractor_id, task_id FROM contractor_task_relation " +
                "WHERE contractor_id = " + contractorId;
        return jdbcTemplate.query(sql, contractorTaskRelationRowMapper);
    }

    public List<ContractorTaskRelation> getContractorTaskRelationByTaskId(int taskId) {
        String sql = "SELECT contractor_id, task_id FROM contractor_task_relation WHERE task_id = :taskId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("taskId", taskId);

        return jdbcTemplate.query(sql, params, contractorTaskRelationRowMapper);
    }

    public int getDoneTasksByContractorId(int contractorId) {
        String sql = "SELECT count(task_id) FROM contractor_task_relation\n" +
                "LEFT JOIN task t on contractor_task_relation.task_id = t.id\n" +
                "WHERE contractor_id = :contractor_id and \"isDone\"=true";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractor_id", contractorId);

        Integer countOfDoneTasks = jdbcTemplate.queryForObject(sql, params, Integer.class);
        if (null == countOfDoneTasks) return 0;
        else return countOfDoneTasks;

    }
}
