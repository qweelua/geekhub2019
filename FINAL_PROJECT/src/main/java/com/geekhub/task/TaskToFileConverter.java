package com.geekhub.task;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.stereotype.Component;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Component
public class TaskToFileConverter {
    private final TaskService taskService;

    public TaskToFileConverter(TaskService taskService) {
        this.taskService = taskService;
    }

    public ByteArrayOutputStream createPdfDocument() throws DocumentException {
        PdfPTable table = new PdfPTable(6);
        table.addCell("Name");
        table.addCell("Short Description");
        table.addCell("Price");
        table.addCell("Is Done");
        table.addCell("Category");
        table.addCell("Date");
        List<Task> allTask = taskService.getAllTask();
        for (Task task : allTask) {
            table.addCell(task.getName());
            table.addCell(task.getShortDescription());
            table.addCell(String.valueOf(task.getPrice()));
            table.addCell(String.valueOf(task.getIsDone()));
            table.addCell(task.getCategory().name());
            table.addCell(String.valueOf(task.getDate()));
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(table);
        document.close();
        return byteArrayOutputStream;
    }

    public Workbook createExcelDocument() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Tasks");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Name");
        row.createCell(1).setCellValue("Short Description");
        row.createCell(2).setCellValue("Price");
        row.createCell(3).setCellValue("Is Done");
        row.createCell(4).setCellValue("Category");
        row.createCell(5).setCellValue("Date");
        int rowIter = 1;
        List<Task> tasks = taskService.getAllTask();
        for (Task task : tasks) {
            Row tempRaw = sheet.createRow(rowIter);
            rowIter++;
            tempRaw.createCell(0).setCellValue(task.getName());
            tempRaw.createCell(1).setCellValue(task.getShortDescription());
            tempRaw.createCell(2).setCellValue(task.getPrice());
            tempRaw.createCell(3).setCellValue(task.getIsDone());
            tempRaw.createCell(4).setCellValue(task.getCategory().getName());
            tempRaw.createCell(5).setCellValue(task.getDate().toString());
        }
        return workbook;
    }

    public XWPFDocument createDocument() {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable();
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("Name");
        tableRowOne.addNewTableCell().setText("Short Description");
        tableRowOne.addNewTableCell().setText("Price");
        tableRowOne.addNewTableCell().setText("Is Done");
        tableRowOne.addNewTableCell().setText("Category");
        tableRowOne.addNewTableCell().setText("Date");
        List<Task> tasks = taskService.getAllTask();
        for (Task task : tasks) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(task.getName());
            tableRowTwo.getCell(1).setText(task.getShortDescription());
            tableRowTwo.getCell(2).setText(String.valueOf(task.getPrice()));
            tableRowTwo.getCell(3).setText(String.valueOf(task.getIsDone()));
            tableRowTwo.getCell(4).setText(task.getCategory().getName());
            tableRowTwo.getCell(5).setText(String.valueOf(task.getDate()));
        }
        return document;
    }
}


