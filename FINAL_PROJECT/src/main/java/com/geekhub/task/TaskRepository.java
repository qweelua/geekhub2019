package com.geekhub.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TaskRowMapper taskRowMapper;

    @Autowired
    public TaskRepository(NamedParameterJdbcTemplate jdbcTemplate, TaskRowMapper taskRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.taskRowMapper = taskRowMapper;
    }

    public Task saveTask(Task task) {
        String sql = "INSERT INTO task(name, \"shortDescription\", price, \"isDone\", category, date)" +
                " VALUES(:name, :shortDescription, :price, :isDone, :category, :date)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", task.getName());
        params.addValue("shortDescription", task.getShortDescription());
        params.addValue("price", task.getPrice());
        params.addValue("isDone", task.getIsDone());
        params.addValue("category", task.getCategory().name());
        params.addValue("date", task.getDate());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder, new String[]{"id"});

        int id = keyHolder.getKey().intValue();

        task.setId(id);

        return task;
    }

    public List<Task> getAllTasks() {
        String sql = "SELECT * FROM task ORDER BY id DESC";
        return jdbcTemplate.query(sql, taskRowMapper);
    }

    public Task getTaskById(int taskId) {
        String sql = "SELECT * FROM task WHERE id = :taskId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("taskId", taskId);

        return jdbcTemplate.queryForObject(sql, params, taskRowMapper);
    }

    public void updateIsDoneByTaskId(int taskId) {
        String sql = "UPDATE task SET \"isDone\"= TRUE WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", taskId);

        jdbcTemplate.update(sql, params);
    }

}
