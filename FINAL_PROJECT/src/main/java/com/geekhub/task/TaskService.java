package com.geekhub.task;

import com.geekhub.customertask.CustomerTaskRelation;
import com.geekhub.customertask.CustomerTaskRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Service
public class TaskService {

    final private TaskRepository taskRepository;
    final private CustomerTaskRelationRepository customerTaskRelationRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, CustomerTaskRelationRepository customerTaskRelationRepository) {
        this.taskRepository = taskRepository;
        this.customerTaskRelationRepository = customerTaskRelationRepository;
    }

    public Task createTask(String name, String shortDescription, int price, boolean isDone,
                           Category category, int customerId, LocalDate date) {
        Task task = new Task();
        task.setName(name);
        task.setShortDescription(shortDescription);
        task.setPrice(price);
        task.setIsDone(isDone);
        task.setCategory(category);
        task.setDate(date);
        taskRepository.saveTask(task);

        CustomerTaskRelation customerTaskRelation = new CustomerTaskRelation();
        customerTaskRelation.setTaskId(task.getId());
        customerTaskRelation.setCustomerId(customerId);
        customerTaskRelationRepository.saveCustomerTaskRelation(customerTaskRelation);

        return task;
    }

    public List<Task> getAllTask() {
        return taskRepository.getAllTasks();
    }

    public Task getTaskById(int taskId) {
        return taskRepository.getTaskById(taskId);
    }

    public void updateTaskIsDone(int taskId) {
        taskRepository.updateIsDoneByTaskId(taskId);
    }
}
