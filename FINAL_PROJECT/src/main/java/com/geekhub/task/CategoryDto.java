package com.geekhub.task;

public enum CategoryDto {
    IT("ІТ послуги"), DESIGN("Дизайн послуги"), DOMESTIC_SERVICES("Побутові послуги"),
    HOME_SERVICES("Послуги по дому"), COURIER_SERVICES("Курєрські послуги"),
    PHOTO_SERVICES("Фото та відео послуги");

    private String name;

    public String getName() {
        return name;
    }

    CategoryDto(String name) {
        this.name = name;
    }
}
