package com.geekhub.task;

public enum Category {
    IT("ІТ послуги"), DESIGN("Дизайн послуги"), DOMESTIC_SERVICES("Побутові послуги"),
    HOME_SERVICES("Послуги по дому"), COURIER_SERVICES("Курєрські послуги"),
    PHOTO_SERVICES("Фото та відео послуги");

    private String name;

    public String getName() {
        return name;
    }

    Category(String name) {
        this.name = name;
    }
}
