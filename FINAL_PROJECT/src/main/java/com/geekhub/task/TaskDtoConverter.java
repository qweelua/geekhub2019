package com.geekhub.task;

import org.springframework.stereotype.Component;

@Component
public class TaskDtoConverter {
    public TaskDto convert(Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setShortDescription(task.getShortDescription());
        taskDto.setPrice(task.getPrice());
        taskDto.setIsDone(task.getIsDone());
        taskDto.setCategory(CategoryDto.valueOf(task.getCategory().name()));
        taskDto.setDate(task.getDate());

        return taskDto;
    }
}
