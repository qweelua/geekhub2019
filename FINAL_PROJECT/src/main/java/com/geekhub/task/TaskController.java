package com.geekhub.task;

import com.geekhub.contractor.Contractor;
import com.geekhub.contractor.ContractorDto;
import com.geekhub.contractor.ContractorDtoConverter;
import com.geekhub.contractor.ContractorService;
import com.geekhub.contractortask.ContractorTaskRelation;
import com.geekhub.contractortask.ContractorTaskRelationService;
import com.geekhub.user.User;
import com.geekhub.user.UserService;
import com.itextpdf.text.DocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TaskController {
    private final TaskService taskService;
    private final TaskDtoConverter taskDtoConverter;
    private final ContractorTaskRelationService contractorTaskRelation;
    private final ContractorService contractorService;
    private final ContractorDtoConverter contractorDtoConverter;
    private final UserService userService;
    private final TaskToFileConverter taskToFileConverter;

    public TaskController(TaskService taskService, TaskDtoConverter taskDtoConverter, ContractorTaskRelationService contractorTaskRelation, ContractorService contractorService, ContractorDtoConverter contractorDtoConverter, UserService userService, TaskToFileConverter taskToFileConverter) {
        this.taskService = taskService;
        this.taskDtoConverter = taskDtoConverter;
        this.contractorTaskRelation = contractorTaskRelation;
        this.contractorService = contractorService;
        this.contractorDtoConverter = contractorDtoConverter;
        this.userService = userService;
        this.taskToFileConverter = taskToFileConverter;
    }

    @GetMapping("/task")
    public String getTasks(Model model) {
        List<Task> tasks = taskService.getAllTask();
        List<TaskDto> tasksDto = new ArrayList<>();
        for (Task task : tasks) {
            TaskDto taskDto = taskDtoConverter.convert(task);
            tasksDto.add(taskDto);
        }
        model.addAttribute("tasks", tasksDto);
        return "task/index";
    }

    @GetMapping("/task/{taskId}/show")
    public String getPageAboutTask(@PathVariable int taskId, Model model) {
        Task task = taskService.getTaskById(taskId);
        TaskDto taskDto = taskDtoConverter.convert(task);
        List<ContractorDto> contractors = new ArrayList<>();
        List<ContractorTaskRelation> contractorTaskRelationByTaskId =
                contractorTaskRelation.getContractorTaskRelationByTaskId(taskId);
        for (ContractorTaskRelation taskRelation : contractorTaskRelationByTaskId) {
            int contractorId = taskRelation.getContractorId();
            Contractor contractor = contractorService.getContractorById(contractorId);
            ContractorDto contractorDto = contractorDtoConverter.convert(contractor);
            contractors.add(contractorDto);
        }
        model.addAttribute("contractors", contractors);
        model.addAttribute("task", taskDto);
        return "task/show";
    }

    @PostMapping("/task/{taskId}/show")
    public String saveContractorTaskRelation(@PathVariable int taskId) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUserByUsername(name);
        Contractor contractor = contractorService.getContractorByUserId(user.getId());
        contractorService.addTaskForContractor(contractor, taskId);
        return "redirect:/contractor/" + contractor.getId() + "/show";
    }

    @GetMapping("/task/pdf")
    public void downloadPDFFile(HttpServletResponse response) throws DocumentException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = taskToFileConverter.createPdfDocument();
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=Tasks.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }

    @GetMapping("/task/doc")
    public void downloadDocFile(HttpServletResponse response) throws IOException {
        XWPFDocument document = taskToFileConverter.createDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Tasks.docx");
        OutputStream os = response.getOutputStream();
        document.write(os);
        os.flush();
        os.close();
    }

    @GetMapping("/task/excel")
    public void downloadExcelFile(HttpServletResponse response) throws IOException {
        Workbook excelDocument = taskToFileConverter.createExcelDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Tasks.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }
}
