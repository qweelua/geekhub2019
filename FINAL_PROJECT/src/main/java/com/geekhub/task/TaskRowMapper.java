package com.geekhub.task;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Component
public class TaskRowMapper implements RowMapper<Task> {

    @Override
    public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
        Task task = new Task();
        task.setId(rs.getInt("id"));
        task.setName(rs.getString("name"));
        task.setShortDescription(rs.getString("shortDescription"));
        task.setPrice(rs.getInt("price"));
        task.setIsDone(rs.getBoolean("isDone"));
        task.setCategory(Category.valueOf(rs.getString("category")));
        task.setDate(LocalDate.parse(rs.getString("date")));
        return task;
    }
}
