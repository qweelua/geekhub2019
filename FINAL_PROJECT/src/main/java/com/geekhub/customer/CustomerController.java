package com.geekhub.customer;

import com.geekhub.contractor.Contractor;
import com.geekhub.contractor.ContractorDto;
import com.geekhub.contractor.ContractorDtoConverter;
import com.geekhub.contractor.ContractorService;
import com.geekhub.customertask.CustomerTaskRelation;
import com.geekhub.customertask.CustomerTaskRelationService;
import com.geekhub.task.*;
import com.geekhub.user.User;
import com.geekhub.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CustomerController {
    private final CustomerService customerService;
    private final CustomerDtoConverter customerDtoConverter;
    private final CustomerTaskRelationService customerTaskRelation;
    private final UserService userService;
    private final TaskDtoConverter taskDtoConverter;
    private final TaskService taskService;
    private final ContractorService contractorService;
    private final ContractorDtoConverter contractorDtoConverter;

    @Autowired
    public CustomerController(CustomerService customerService, CustomerDtoConverter customerDtoConverter,
                              CustomerTaskRelationService customerTaskRelation, UserService userService, TaskDtoConverter taskDtoConverter, TaskService taskService, ContractorService contractorService, ContractorDtoConverter contractorDtoConverter) {
        this.customerService = customerService;
        this.customerDtoConverter = customerDtoConverter;
        this.customerTaskRelation = customerTaskRelation;
        this.userService = userService;
        this.taskDtoConverter = taskDtoConverter;
        this.taskService = taskService;
        this.contractorService = contractorService;
        this.contractorDtoConverter = contractorDtoConverter;
    }

    @GetMapping("/customer")
    public String getAllCustomers(Model model) {
        List<Customer> customers = customerService.getAllCustomers();
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer customer : customers) {
            CustomerDto customerDto = customerDtoConverter.convert(customer);
            customerDtos.add(customerDto);
        }
        model.addAttribute("customers", customerDtos);
        return "customer/index";
    }

    @GetMapping("/customer/tasks")
    public String getCustomersTask(Model model) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUserByUsername(name);
        Customer customer = customerService.getCustomerByUserId(user.getId());

        List<CustomerTaskRelation> customerTaskRelationByCustomerId =
                customerTaskRelation.getCustomerTaskRelationByCustomerId(customer.getId());
        List<TaskDto> taskDtos = new ArrayList<>();
        for (CustomerTaskRelation taskRelation : customerTaskRelationByCustomerId) {
            int taskId = taskRelation.getTaskId();
            TaskDto taskDto = taskDtoConverter.convert(taskService.getTaskById(taskId));
            taskDtos.add(taskDto);
        }
        model.addAttribute("tasks", taskDtos);
        return "customer/show";
    }

    @PostMapping("/customer/tasks")
    public String makeTaskDone(@RequestParam int taskId) {
        taskService.updateTaskIsDone(taskId);
        return "redirect:/customer/tasks";
    }

    @GetMapping("/customer/create-task")
    public String getCreateTaskPage(Model model) {
        model.addAttribute("categories", CategoryDto.values());
        return "task/create";
    }

    @PostMapping(value = "/customer/create-task", produces = "application/json")
    public @ResponseBody
    ContractorDto createTask(@RequestBody Task task) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUserByUsername(username);
        Customer customer = customerService.getCustomerByUserId(user.getId());
        taskService.createTask(task.getName(), task.getShortDescription(), task.getPrice(),
                false, task.getCategory(), customer.getId(), task.getDate());

        List<Contractor> contractors = contractorService.getContractorsWhoFreeByDay(task.getDate().toString());
        List<ContractorDto> contractorDtos = new ArrayList<>();
        for (Contractor contractor : contractors) {
            ContractorDto contractorDto = contractorDtoConverter.convert(contractor);
            contractorDtos.add(contractorDto);
        }
        ContractorDto relatedContractor = contractorDtos
                .stream()
                .filter(obj -> obj.getWorkCategory().toString().equals(task.getCategory().toString()))
                .reduce((first, second) -> second)
                .orElse(null);
        return relatedContractor;
    }

}
