package com.geekhub.customer;

import com.geekhub.client.GenderDto;
import com.geekhub.client.CityDto;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoConverter {

    public CustomerDto convert(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setName(customer.getName());
        customerDto.setSurname(customer.getSurname());
        customerDto.setAge(customer.getAge());
        customerDto.setGender(GenderDto.valueOf(customer.getGender().name()));
        customerDto.setCity(CityDto.valueOf(customer.getCity().name()));
        customerDto.setUserId(customer.getUserId());

        return customerDto;
    }
}
