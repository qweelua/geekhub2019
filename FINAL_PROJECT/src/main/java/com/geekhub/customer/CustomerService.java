package com.geekhub.customer;

import com.geekhub.client.ClientService;
import com.geekhub.client.Gender;
import com.geekhub.client.City;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CustomerService extends ClientService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer createCustomer(String name, String surname, int age, Gender gender, City city, int userId) {
        Customer customer = new Customer();
        customer.setName(name);
        customer.setSurname(surname);
        customer.setAge(age);
        customer.setGender(gender);
        customer.setCity(city);
        customer.setUserId(userId);

        return customerRepository.saveCustomer(customer);
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    public Customer getCustomerById(int customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    public Customer getCustomerByUserId(int userId) {
        return customerRepository.getCustomerByUserId(userId);
    }
}
