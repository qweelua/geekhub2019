package com.geekhub.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CustomerRowMapper customerRowMapper;

    @Autowired
    public CustomerRepository(NamedParameterJdbcTemplate jdbcTemplate, CustomerRowMapper customerRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.customerRowMapper = customerRowMapper;
    }

    public Customer saveCustomer(Customer customer) {
        String sql = "INSERT INTO customer" +
                "(name, surname, age, gender, city)" +
                " VALUES (:username, :user_id, :surname, :age, :gender, :city)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id", customer.getUserId());
        params.addValue("name", customer.getName());
        params.addValue("surname", customer.getSurname());
        params.addValue("age", customer.getAge());
        params.addValue("gender", customer.getGender().name());
        params.addValue("city", customer.getCity().name());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        customer.setId(generatedId);

        return customer;
    }

    public List<Customer> getAllCustomers() {
        String sql = "SELECT * FROM customer";
        return jdbcTemplate.query(sql, customerRowMapper);
    }

    public Customer getCustomerById(int customerId) {
        String sql = "SELECT * FROM customer WHERE id = :customerId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("customerId", customerId);

        return jdbcTemplate.queryForObject(sql, params, customerRowMapper);
    }

    public Customer getCustomerByUserId(int userId) {
        String sql = "SELECT * FROM customer WHERE user_id = :userId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userId);

        return jdbcTemplate.queryForObject(sql, params, customerRowMapper);
    }
}
