package com.geekhub.list.linked;

import com.geekhub.list.List;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;

    private int size = 0;

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        if (element == null) return false;
        if (tail == null) {
            head = node;
        } else tail.next = node;

        tail = node;
        size++;
        return true;
    }

    @Override
    public boolean add(int index, E element) {
        Node<E> node = new Node<>(element, null);
        if (element == null) return false;
        if (index > size) return false;
        if (index == 0) {
            node.next = head;
            head = node;
        }
        if (head == tail) {
            tail.next = node;
            tail = node;
        }
        int counter = 0;
        Node current = head;
        while (current.next != null) {
            counter++;
            if (counter == index) {
                node.next = current.next;
                current.next = node;
            } else current = current.next;
        }
        size++;
        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        Iterator<E> iterator = elements.iterator();
        if (elements.size() == 0) return false;
        if (tail == null) {
            while (iterator.hasNext()) {
                head = new Node<>(iterator.next(), null);
                size++;
            }
        } else {
            while (iterator.hasNext()) {
                Node<E> node = new Node<>(iterator.next(), null);
                tail.next = node;
                tail = node;
                size++;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        Iterator<E> iterator = elements.iterator();
        if (index > size) return false;
        if (elements.size() == 0) return false;
        if (index == 0) {
            Node<E> node = new Node<>(iterator.next(), head);
            head = node;
            size++;
        }
        if (index == size) {
            while (iterator.hasNext()) {
                Node<E> node = new Node<>(iterator.next(), null);
                tail.next = node;
                tail = node;
                size++;
            }
        }
        int counter = 0;
        Node<E> current = head;
        while (counter < index - 1) {
            current = current.next;
            counter++;
        }
        while (iterator.hasNext()) {
            Node<E> node = new Node<>(iterator.next(), current.next);
            current.next = node;
            current = node;
            size++;
        }
        return true;
    }

    @Override
    public boolean clear() {
        head = null;
        tail = null;
        size = 0;
        return true;
    }

    @Override
    public E remove(int index) {
        Node<E> current = head;
        E elementToRemove = null;
        if (index > size) return null;
        if (head == tail) {
            elementToRemove = head.element;
            head = null;
            tail = null;
            size--;
            return elementToRemove;
        }
        if (index == 0) {
            elementToRemove = head.element;
            head = head.next;
            size--;
            return elementToRemove;
        }
        int counter = 0;
        while (counter < index - 1) {
            current = current.next;
            counter++;
        }
        if (current.next == tail) {
            elementToRemove = current.next.element;
            tail = current;
            tail.next = null;
            size--;
            return elementToRemove;
        }
        elementToRemove = current.next.element;
        current.next = current.next.next;
        size--;
        return elementToRemove;
    }

    @Override
    public E remove(E element) {
        E elementToRemove = null;
        if (head == null) return null;
        if (head == tail) {
            elementToRemove = head.element;
            head = null;
            tail = null;
            return elementToRemove;
        }
        if (head.element == element) {
            elementToRemove = head.element;
            head = head.next;
            size--;
            return elementToRemove;
        }
        Node<E> current = head;
        while (current.next != null) {
            if (current.next.element.equals(element)) {
                if (tail.equals(current.next)) {
                    elementToRemove = tail.element;
                    tail = current.next;
                    size--;
                    return elementToRemove;
                }
                elementToRemove = current.next.element;
                current.next = current.next.next;
                size--;
                return elementToRemove;
            } else current = current.next;
        }
        return null;
    }

    @Override
    public E get(int index) {
        int counter = 0;
        Node<E> current = head;
        while (current != null) {
            if (counter == index) return current.element;
            current = current.next;
            counter++;
        }
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    @Override
    public int indexOf(E element) {
        Node current = head;
        int counter = 0;
        while (current != null) {
            if (element.equals(current.element)) {
                return counter;
            } else current = current.next;
            counter++;
        }
        return -1;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }
}