create table users
(
    id      serial   not null,
    name    char(15) not null,
    message varchar,
    rate    integer  not null
);

alter table users
    owner to postgres;

