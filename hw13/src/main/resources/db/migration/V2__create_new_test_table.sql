create table test
(
    id      serial   not null,
    name    char(15) not null,
    isTested bool not null
);