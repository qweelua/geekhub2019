package com.geekhub.dao;

import com.geekhub.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Repository
public class FeedBackRepository implements Repository {
    public DataSource hikariDataSource;

    @Autowired
    public FeedBackRepository(DataSource dataSource) {
        this.hikariDataSource = dataSource;
    }


    @Override
    public void createUser(String name, String message, int rate, String date) throws SQLException {
        String sql = "INSERT INTO users (name, message, rate, date) VALUES (?,?,?,?)";
        try (Connection connection = hikariDataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, message);
                preparedStatement.setInt(3, rate);
                preparedStatement.setString(4, date);
                preparedStatement.execute();
            }
        }
    }

    @Override
    public User getUserById(int id) throws SQLException {
        User user = new User();
        String sql = "SELECT * FROM users WHERE id = " + id;
        try (Connection connection = hikariDataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    user.setId(resultSet.getInt("id"));
                    user.setName(resultSet.getString("name"));
                    user.setMessage(resultSet.getString("message"));
                    user.setRate(resultSet.getInt("rate"));
                    user.setLocalDateTime(LocalDateTime.parse(resultSet.getString("date")));
                }
            }
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM users ORDER BY users.public.users.date DESC";
        try (Connection connection = hikariDataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getInt("id"));
                    user.setName(resultSet.getString("name"));
                    user.setMessage(resultSet.getString("message"));
                    user.setRate(resultSet.getInt("rate"));
                    user.setLocalDateTime(LocalDateTime.parse(resultSet.getString("date")));
                    users.add(user);

                }
            }
            return users;
        }
    }

    public List<Integer> pagesNumbers() throws SQLException {
        List<Integer> pages = new ArrayList<>();
        List<User> users = getAllUsers();
        for (int i = 1; i <= users.size() / 10 + 1; i++) {
            pages.add(i);
        }
        return pages;
    }

    public List<User> getListOfUsersOnPage(int page) throws SQLException {
        List<User> users = getAllUsers();
        if (page >= pagesNumbers().size()) {
            return users.subList(page * 10 - 10, users.size());
        } else {
            return users.subList(page * 10 - 10, page * 10);
        }
    }
}
