package com.geekhub.dao;

import com.geekhub.model.User;

import java.sql.SQLException;
import java.util.List;


public interface Repository {

    void createUser(String name, String message, int rate, String date) throws SQLException;

    User getUserById(int id) throws SQLException;

    List<User> getAllUsers() throws SQLException;

}
