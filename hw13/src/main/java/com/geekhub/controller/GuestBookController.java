package com.geekhub.controller;

import com.geekhub.dao.FeedBackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.time.LocalDateTime;

@Controller
public class GuestBookController {

    FeedBackRepository feedBackRepository;

    @Autowired
    public void setFeedBackRepository(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }


    @GetMapping("/guestbook")
    public String guestBookView(@RequestParam(name = "page", defaultValue = "1") String page, Model model) throws SQLException {
        model.addAttribute("pages", feedBackRepository.pagesNumbers());
        model.addAttribute("users", feedBackRepository.getListOfUsersOnPage(Integer.parseInt(page)));
        return "guestbook";
    }

    @PostMapping("/guestbook")
    public String addUserMessage(@RequestParam String name,
                               @RequestParam String message,
                               @RequestParam String rate) throws SQLException {
        if (!name.isEmpty()) {
            feedBackRepository.createUser(name, message, Integer.parseInt(rate), LocalDateTime.now().toString());
        }
        return "redirect:/guestbook";
    }

}
