package com.geekhub.model;

import java.time.LocalDateTime;

public class User {
    private int id;
    private String name;
    private String message;
    private int rate;
    LocalDateTime localDateTime;

    public User() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public int getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "User: " + "\n" +
                "id= " + id + "\n" +
                ", name= ' " + name + '\'' + "\n" +
                ", message = '" + message + '\'' + "\n" +
                ", rate = " + rate;
    }
}
