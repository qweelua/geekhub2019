package com.geekhub.task2.imagedownloader;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    private final ExecutorService executorService;
    private final String folder;

    public ImageCrawler(int numberOfThreads, String folder) {
        executorService = Executors.newFixedThreadPool(numberOfThreads);
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     *
     * @param urlToPage
     * @throws java.io.IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        Collection<URL> imageLinks = page.getImageLinks();
        for (URL imageLink : imageLinks) {
            if (isImageURL(imageLink)) {
                executorService.execute(new ImageTask(imageLink, folder));
            }
        }

    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    private boolean isImageURL(URL url) {
        return url.toString().matches("([^\\s]+(\\.(?i)(jpg|png))$)");
    }
}