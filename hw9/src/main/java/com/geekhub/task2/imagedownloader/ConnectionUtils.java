package com.geekhub.task2.imagedownloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ConnectionUtils {

    private ConnectionUtils() {
    }

    public static byte[] getData(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        urlConnection.getInputStream()))) {
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        }
        return stringBuilder.toString().getBytes();
    }
}
