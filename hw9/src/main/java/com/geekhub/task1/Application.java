package com.geekhub.task1;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        File result = new File("hw9\\result\\result.txt");
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(
                        new File("hw9\\source\\source.txt")))) {
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                executorService.execute(new Hasher(read, result));
            }
        }
        executorService.shutdown();
    }
}