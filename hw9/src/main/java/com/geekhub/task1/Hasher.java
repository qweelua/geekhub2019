package com.geekhub.task1;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Hasher implements Runnable {
    String page;
    File result;

    public Hasher(String page, File result) {
        this.page = page;
        this.result = result;
    }

    @Override
    public void run() {

        String contentOfHTTPPage = null;
        try {
            contentOfHTTPPage = getContentOfHTTPPage(page);
        } catch (Exception e) {
            System.out.println("Exception!");
        }
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException!");
        }
        StringBuilder stringBuilder = hashToMd5(contentOfHTTPPage, md5);
        writeToFile(stringBuilder);
    }

    private StringBuilder hashToMd5(String contentOfHTTPPage, MessageDigest md5) {
        byte[] bytes = md5.digest(contentOfHTTPPage.getBytes());
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02X", b));
        }
        stringBuilder.append("\n");
        return stringBuilder;
    }

    private void writeToFile(StringBuilder stringBuilder) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(result, true));
            bufferedWriter.write(stringBuilder.toString());
        } catch (IOException e) {
            System.out.println("IOException!");
        }
    }

    public static String getContentOfHTTPPage(String pageAddress) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        URL pageURL = new URL(pageAddress);
        URLConnection urlConnection = pageURL.openConnection();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        urlConnection.getInputStream()))) {
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        }
        return stringBuilder.toString();
    }
}