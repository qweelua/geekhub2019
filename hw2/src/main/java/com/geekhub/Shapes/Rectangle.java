package com.geekhub.Shapes;

public class Rectangle implements Shape {
    float a;
    float b;

    public Rectangle(float a, float b) {
        this.a = a;
        this.b = b;
    }

    public double calculatePerimeter() {
        float perimetr = 2 * (a + b);
        return perimetr;
    }

    public double calculateArea() {
        float area = a * b;
        return area;
    }

    public Triangle getTriangle() {
        Triangle triangle = new Triangle(a, b, (float) Math.sqrt(a * a + b * b));
        return triangle;
    }
}