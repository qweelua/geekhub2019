package com.geekhub.Shapes;

public class Triangle implements Shape {
    float a;
    float b;
    float c;

    public Triangle(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculatePerimeter() {
        float perimetr = a + b + c;
        return perimetr;
    }

    public double calculateArea() {
        double halfPerimetr = 0.5 * (a + b + c);
        double area = Math.sqrt(halfPerimetr * (halfPerimetr - a) * (halfPerimetr - b) * (halfPerimetr - c));
        return area;
    }
}

