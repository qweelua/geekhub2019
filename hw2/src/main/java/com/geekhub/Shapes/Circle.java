package com.geekhub.Shapes;

public class Circle implements Shape {

    float radius;

    public Circle(float radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        double perimetr = 2 * 3.14 * radius;
        return perimetr;
    }

    @Override
    public double calculateArea() {
        double area = (3.14 * radius) * (3.14 * radius);
        return area;
    }

}
