package com.geekhub.Shapes;

public class Square implements Shape {
    float a;

    public Square(float a) {
        this.a = a;
    }

    public double calculatePerimeter() {
        float perimetr = 4 * a;
        return perimetr;
    }

    public double calculateArea() {
        float area = a * a;
        return area;

    }

    public Triangle getTriangle() {
        Triangle triangle = new Triangle(a, a, (float) (a * Math.sqrt(2)));
        return triangle;
    }
}