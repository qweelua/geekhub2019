package com.geekhub.Shapes;

public interface Shape {
    double calculateArea();

    double calculatePerimeter();
}
