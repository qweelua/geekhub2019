package com.geekhub;

import com.geekhub.Shapes.*;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Figures[] shapes = Figures.values();
        System.out.println("Choose the one of:" + Arrays.toString(shapes));
        String inputForm = in.nextLine().toUpperCase();
        boolean isFigures = true;
        while (isFigures) {
            for (Figures figures : Figures.values()) {
                if (figures.name().equals(inputForm)) {
                    isFigures = false;
                }
            }
            if (isFigures) {
                System.out.println("Choose the one of:" + Arrays.toString(shapes));
                inputForm = in.nextLine().toUpperCase();
            }
        }
        Figures figures = Figures.valueOf(inputForm);
        switch (figures) {
            case CIRCLE:
                calculateCircle(in);
                break;
            case SQUARE:
                calculateSquare(in);
                break;
            case RECTANGLE:
                calculateRectangle(in);
                break;
            case TRIANGLE:
                calculateTriangle(in);
                break;
            default:
                System.out.println("This shape is not available");
                break;
        }
    }

    private static void calculateCircle(Scanner in) {
        System.out.println("Enter radius of your circle\nradius = ");
        float radius = in.nextFloat();
        Shape circle;
        circle = new Circle(radius);
        System.out.println("Area of Circle = " + circle.calculateArea() +
                ". Perimetr of Circle = " + circle.calculatePerimeter());
    }

    private static void calculateSquare(Scanner in) {
        System.out.println("Enter the length of one side of your square\na = ");
        float aSideSquare = in.nextFloat();
        Square square;
        square = new Square(aSideSquare);
        System.out.println("Area of Square = " + square.calculateArea() +
                ". Perimetr of Square = " + square.calculatePerimeter() +
                "Area of Triangles in Square = " + square.getTriangle().calculateArea()
                + "Perimetr of Triangels in Square = " + square.getTriangle().calculatePerimeter());
    }

    private static void calculateTriangle(Scanner in) {
        System.out.println("Enter the length of three sides of your Triangle\na,b and c = ");
        float aSideTriangle = in.nextFloat();
        float bSideTriangle = in.nextFloat();
        float cSideTriangle = in.nextFloat();
        Shape triangle;
        triangle = new Triangle(aSideTriangle, bSideTriangle, cSideTriangle);
        System.out.println("Area of triangle = " + triangle.calculateArea() +
                ". Perimetr of triangle = " + triangle.calculatePerimeter());
    }

    private static void calculateRectangle(Scanner in) {
        System.out.println("Enter the length of two sides of your Rectangle\na and b = ");
        float aSideRectangle = in.nextFloat();
        float bSideRectangle = in.nextFloat();
        Rectangle rectangle;
        rectangle = new Rectangle(aSideRectangle, bSideRectangle);
        System.out.println("Area of Rectangle = " + rectangle.calculateArea() +
                ". Perimetr of Rectangle = " + rectangle.calculatePerimeter() +
                "Area of Triangles in Rectangle = " + rectangle.getTriangle().calculateArea()
                + "Perimetr of Triangels in Square = " + rectangle.getTriangle().calculatePerimeter());
    }
}