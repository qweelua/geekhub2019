package com.geekhub.task4.task;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class TaskManager {
    public TaskManager() {
    }

    public static List<Task> find5NearestImportantTasks(List<Task> tasks) {
        List<Task> fiveImportentTasks = tasks.stream()
                .filter(task -> task.type == TaskType.IMPORTANT && !task.done && task.getStarsDate().equals(LocalDate.now()))
                .limit(5)
                .collect(Collectors.toList());
        fiveImportentTasks.forEach(System.out::println);
        return fiveImportentTasks;
    }

    public static List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .map(task -> task.getCategories())
                .flatMap(category -> category.stream())
                .distinct()
                .collect(Collectors.toList());
    }

    public static Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .map(task -> task.getCategories())
                .flatMap(category -> category.stream())
                .distinct()
                .collect(Collectors.toMap(key -> key, key -> tasks.stream()
                        .filter(value -> value.getCategories().contains(key))
                        .collect(Collectors.toList())));
    }

    public static Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream().collect(Collectors.partitioningBy(task -> task.done));
    }

    public static boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream().anyMatch(task -> task.categories.contains(category));
    }

    public static String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream().filter(task -> task.id >= startNo && task.id <= endNo)
                .map(task -> task.getTitle())
                .collect(Collectors.joining(" and ", "In range " + startNo + "-" + endNo + " ", " tasks"));
    }

    public static Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream().collect(Collectors.groupingBy(task -> task.getCategories().toString(), Collectors.counting()));
    }

    public static IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream().collect(Collectors.summarizingInt(task -> task.title.length()));
    }

    public static Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream().max(Comparator.comparingInt(task -> task.categories.size())).get();
    }
}
