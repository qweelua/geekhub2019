package com.geekhub.task4.task;

import java.time.LocalDate;
import java.util.Set;

public class Task {
    int id;
    TaskType type;
    String title;
    boolean done;
    Set<String> categories;
    LocalDate startsOn;

    public Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;
    }

    public String getTitle() {
        return title;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public LocalDate getStarsDate() {
        return startsOn;
    }

    @Override
    public String toString() {
        return "Task{" +
                "type=" + type +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", startsOn = " + startsOn +
                '}';
    }

}
