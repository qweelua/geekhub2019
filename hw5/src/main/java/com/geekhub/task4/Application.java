package com.geekhub.task4;

import com.geekhub.task4.task.Task;
import com.geekhub.task4.task.TaskManager;
import com.geekhub.task4.task.TaskType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Application {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.of(2019, 11, 16);
        Set<String> categories1 = new HashSet<>();
        Set<String> categories2 = new HashSet<>();
        Set<String> categories3 = new HashSet<>();
        categories1.add("Category1");
        categories1.add("Category2");
        categories1.add("Category3");
        categories1.add("Category4");
        categories2.add("Category2.1");
        categories2.add("Category2.2");
        categories2.add("Category2.3");
        categories3.add("Category3.1");
        categories3.add("Category3.2");
        Task task1 = new Task(1, TaskType.IMPORTANT, "Do smth1", false, categories3, date1);
        Task task2 = new Task(2, TaskType.IMPORTANT, "Do smth2", true, categories2, LocalDate.of(2019, 11, 16));
        Task task3 = new Task(3, TaskType.IMPORTANT, "Do smth3", false, categories2, LocalDate.of(2019, 11, 16));
        Task task4 = new Task(4, TaskType.IMPORTANT, "Do smth4", false, categories3, date1);
        Task task5 = new Task(5, TaskType.IMPORTANT, "Do smth5", false, categories3, date1);
        Task task6 = new Task(6, TaskType.IMPORTANT, "Do smth6", true, categories2, date1);
        Task task7 = new Task(7, TaskType.MIDDLE, "Do smth7", true, categories1, date1);
        List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        tasks.add(task5);
        tasks.add(task6);
        tasks.add(task7);

        testMethods(tasks);
    }

    private static void testMethods(List<Task> tasks) {
        System.out.println("[TASK 1]");
        TaskManager.find5NearestImportantTasks(tasks);
        System.out.println("[TASK 2]");
        System.out.println(TaskManager.getUniqueCategories(tasks));
        System.out.println("[TASK 3]");
        System.out.println(TaskManager.getCategoriesWithTasks(tasks));
        System.out.println("[TASK 4]");
        System.out.println(TaskManager.splitTasksIntoDoneAndInProgress(tasks));
        System.out.println("[TASK 5]");
        System.out.println(TaskManager.existsTaskOfCategory(tasks, "Category2.1"));
        System.out.println(TaskManager.existsTaskOfCategory(tasks, "Category22"));
        System.out.println("[TASK 6]");
        System.out.println(TaskManager.getTitlesOfTasks(tasks, 2, 6));
        System.out.println("[TASK 7]");
        System.out.println(TaskManager.getCountsByCategories(tasks));
        System.out.println("[TASK 8]");
        System.out.println(TaskManager.getCategoriesNamesLengthStatistics(tasks));
        System.out.println("[TASK 9]");
        System.out.println(TaskManager.findTaskWithBiggestCountOfCategories(tasks));
    }
}
