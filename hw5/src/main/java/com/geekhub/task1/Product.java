package com.geekhub.task1;

import java.util.Objects;

public class Product {
    int price;
    String name;
    int quantity;

    public Product(int price, String name, int quantity) {
        this.price = price;
        this.name = name;
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return price == product.price &&
                quantity == product.quantity &&
                name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name, quantity);
    }
}
