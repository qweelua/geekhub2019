package com.geekhub.task1;

import java.util.ArrayList;

public class Inventory {
    ArrayList<Product> products;

    public Inventory(ArrayList<Product> products) {
        this.products = products;
    }

    public void addProduct(int price, String name, int quantity) {
        Product product = new Product(price, name, quantity);
        products.add(product);
    }

    public int calculateWholeSum() {
        int sum = 0;
        for (Product i : products) {
            sum += i.price * i.quantity;
        }
        return sum;
    }
}
