package com.geekhub.task1;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;


public class Application {
    public static void main(String[] args) {
        Product product1 = new Product(15, "Baton", 10);
        Product product2 = new Product(25, "Coca-cola", 15);
        Product product3 = new Product(25, "Coca-cola", 5);
        Product product4 = new Product(27, "Pepsi", 0);
        Product product5 = new Product(25, "Sprite", 0);
        Product product6 = new Product(27, "Pepsi", 10);
        Product product7 = new Product(27, "Pepsi", 15);
        ArrayList<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        products.add(product5);
        products.add(product6);
        products.add(product7);
        printQuantityOnHands(products);
        productsToMap(products);
        Inventory inventory = new Inventory(products);
        addProduct(inventory);
        System.out.println("Whole sum = " + getWholeSum(products));
    }

    private static void addProduct(Inventory inventory) {
        System.out.println("[ADD PRODUCT]");
        Scanner in = new Scanner(System.in);
        System.out.println("Enter price of product ");
        int price = in.nextInt();
        System.out.println("Enter name of product ");
        String name;
        name = in.next();
        System.out.println("Enter quantity of product ");
        int quantity = in.nextInt();
        inventory.addProduct(price, name, quantity);
    }

    private static void productsToMap(ArrayList<Product> products) {
        Map<String, Integer> productsMap = products.stream()
                .collect(Collectors.toMap(Product::getName, Product::getQuantity, Integer::sum));
        System.out.println(productsMap);
    }

    private static void printQuantityOnHands(ArrayList<Product> products) {
        products.stream()
                .filter(product -> product.getQuantity() > 0)
                .map(Product::getName)
                .distinct()
                .forEach(name -> System.out.println(name + " on hands"));
    }

    private static int getWholeSum(ArrayList<Product> products) {
        return products
                .stream()
                .mapToInt(product -> product.getQuantity() * product.getPrice())
                .sum();
    }
}
