package com.geekhub.task2.user;

public class User implements Comparable<User> {

    private final String name;
    private final String age;

    public User(String name, String age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(User user) {
        int result = this.age.compareTo(user.age);
        if (result == 0) {
            result = this.name.compareTo(user.name);
        }
        return result;
    }


    @Override
    public String toString() {
        return "User{" +
                "age='" + age + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

