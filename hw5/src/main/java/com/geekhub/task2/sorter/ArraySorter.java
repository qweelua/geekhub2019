package com.geekhub.task2.sorter;

public class ArraySorter {
    public static <T extends Comparable<T>> T[] bubbleSort(T[] array, Direction direction) {
        T[] sortedArray = array;
        T temp;
        boolean sorted = true;
        for (int j = 1; j < sortedArray.length & sorted; j++) {
            sorted = false;
            for (int i = 0; i < sortedArray.length - j; i++) {
                if (sortedArray[i].compareTo(sortedArray[i + 1]) * direction.getDirection() > 0) {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i + 1];
                    sortedArray[i + 1] = temp;
                    sorted = true;
                }
            }
        }
        return sortedArray;
    }

    public static <T extends Comparable<T>> T[] insertionSort(T[] array, Direction direction) {
        T[] sortedArray = array;
        T temp;
        for (int i = 1; i < sortedArray.length; i++) {
            for (int j = i; (j > 0) && (sortedArray[j].compareTo(sortedArray[j - 1]) * direction.getDirection() < 0); j--) {
                temp = sortedArray[j];
                sortedArray[j] = sortedArray[j - 1];
                sortedArray[j - 1] = temp;
            }
        }
        return sortedArray;
    }

    public static <T extends Comparable<T>> T[] selectionSort(T[] array, Direction direction) {
        T[] sortedArray = array;
        int minIndex;
        T temp;
        for (int i = 0; i < sortedArray.length; i++) {
            minIndex = i;
            for (int j = i + 1; j < sortedArray.length; j++) {
                if (sortedArray[minIndex].compareTo(sortedArray[j]) * direction.getDirection() > 0) {
                    minIndex = j;
                }
            }
            temp = sortedArray[minIndex];
            sortedArray[minIndex] = sortedArray[i];
            sortedArray[i] = temp;
        }
        return sortedArray;
    }
}


