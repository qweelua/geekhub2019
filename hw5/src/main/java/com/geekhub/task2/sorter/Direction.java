package com.geekhub.task2.sorter;

public enum Direction {

    ASC(1), DESC(-1);

    private int direction;

    Direction(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return this.direction;
    }
}
