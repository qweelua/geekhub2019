package com.geekhub.task2;

import com.geekhub.task2.sorter.ArraySorter;
import com.geekhub.task2.sorter.Direction;
import com.geekhub.task2.user.User;

public class Application {
    public static void main(String[] args) {
        User[] users = new User[3];
        users[0] = new User("Andrey", "8");
        users[1] = new User("Bndrey", "8");
        users[2] = new User("Grisha", "5");
        testBubbleSort(users);
        testInversionSort(users);
        testSelectionSort(users);
    }

    private static void testBubbleSort(User[] users) {
        ArraySorter.bubbleSort(users, Direction.ASC);
        System.out.println("[BUBBLE SORT ASC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
        ArraySorter.bubbleSort(users, Direction.DESC);
        System.out.println("[BUBBLE SORT DESC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
    }

    private static void testInversionSort(User[] users) {
        ArraySorter.insertionSort(users, Direction.ASC);
        System.out.println("[INSERTION SORT ASC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
        ArraySorter.insertionSort(users, Direction.DESC);
        System.out.println("[INSERTION SORT DESC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
    }

    private static void testSelectionSort(User[] users) {
        ArraySorter.selectionSort(users, Direction.ASC);
        System.out.println("[SELECTION SORT ASC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
        ArraySorter.selectionSort(users, Direction.DESC);
        System.out.println("[SELECTION SORT DESC]");
        for (User user : users) {
            System.out.println(user.toString());
        }
    }
}
