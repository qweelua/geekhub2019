package com.geekhub.task3;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CollectionUtils {

    CollectionUtils() {
    }

    public static <E> List<E> generate(Supplier<E> generator,
                                       Supplier<List<E>> listFactory,
                                       int count) {
        List<E> elements = listFactory.get();
        for (int i = 0; i < count; i++) {
            elements.add(generator.get());
        }
        return elements;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        if (elements.isEmpty()) return null;
        elements.removeIf(o -> !filter.test(o));
        return elements;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.isEmpty()) return false;
        for (E element : elements) {
            return predicate.test(element);
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.isEmpty()) return false;
        for (E element : elements) {
            if (!predicate.test(element)) return false;
        }
        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.isEmpty()) return true;
        for (E element : elements) {
            if (predicate.test(element)) return false;
        }
        return true;
    }

    public static <T, R> List<R> map(List<T> elements,
                                     Function<T, R> mappingFunction,
                                     Supplier<List<R>> listFactory) {
        List<R> newElements = listFactory.get();
        for (T element : elements) {
            newElements.add(mappingFunction.apply(element));
        }
        return newElements;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E maxElement = iterator.next();
        while (iterator.hasNext()) {
            E element = iterator.next();
            if (comparator.compare(element, maxElement) > 0) maxElement = element;
        }
        return Optional.of(maxElement);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E minElement = iterator.next();
        while (iterator.hasNext()) {
            E element = iterator.next();
            if (comparator.compare(element, minElement) < 0) minElement = element;
        }
        return Optional.of(minElement);
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> elementsIsDistinct = listFactory.get();
        Set<E> set = new HashSet<>();
        set.addAll(elements);
        elementsIsDistinct.addAll(set);
        return elementsIsDistinct;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) return Optional.empty();
        Iterator<E> iterator = elements.iterator();
        E result = iterator.next();
        while (iterator.hasNext()) {
            E element = iterator.next();
            result = accumulator.apply(result, element);
        }
        return Optional.of(result);
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
        for (E element : elements)
            result = accumulator.apply(result, element);
        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                        Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {
        Map<Boolean, List<E>> map = mapFactory.get();
        map.put(Boolean.TRUE, listFactory.get());
        map.put(Boolean.FALSE, listFactory.get());
        if (elements.isEmpty()) return map;
        for (E element : elements) {
            map.get(predicate.test(element)).add(element);
        }
        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                                 Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory,
                                                 Supplier<List<T>> listFactory) {
        Map<K, List<T>> map = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            if (!map.containsKey(key)) {
                map.put(key, listFactory.get());
            }
            map.get(key).add(element);
        }
        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction,
                                            Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            if (map.containsKey(key)) {
                map.put(key, mergeFunction.apply(map.get(key), valueFunction.apply(element)));
            } else {
                map.put(key, valueFunction.apply(element));
            }
        }
        return map;
    }

    //ADDITIONAL METHODS

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                        Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        Map<Boolean, List<T>> map = mapFactory.get();
        map.put(Boolean.TRUE, listFactory.get());
        map.put(Boolean.FALSE, listFactory.get());
        for (E element : elements) {
            map.get(predicate.test(element)).add(elementMapper.apply(element));
        }
        return map;
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                                 Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        Map<K, List<U>> map = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            if (!map.containsKey(key)) {
                map.put(key, listFactory.get());
            }
            map.get(key).add(elementMapper.apply(element));
        }
        return map;
    }
}
