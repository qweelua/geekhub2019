package com.geekhub.task3;

import java.util.*;
import java.util.function.*;

public class Application {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(-1);
        numbers.add(-1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(6);
        numbers.add(-12);
        Predicate<Integer> isPositive = x -> x > 0;
        Predicate<Integer> isNegative = x -> x < 0;
        BinaryOperator<Integer> accumulator = (x, y) -> x + y;
        BinaryOperator<String> accumulatorStr = (x, y) -> x + " " + y;
        Function<Integer, String> convertToString = (x) -> "intNum " + x;
        Function<Integer, String> convertToStringSum = (x) -> " sum to double " + x;
        Function<Integer, Double> convertToDouble = (x) -> (double) (x + x);
        Supplier<Integer> random = () -> (int) (Math.random() * 10);
        Supplier<List<Integer>> arrayListIntegerSupplier = ArrayList::new;
        Supplier<List<Double>> arrayListDoubleSupplier = ArrayList::new;
        Supplier<List<String>> arrayListStringSupplier = ArrayList::new;
        Supplier<Map<Boolean, List<Integer>>> hashMapBooleanIntegerSupplier = HashMap::new;
        Supplier<Map<String, List<Integer>>> hashMapStringIntegerSupplier = HashMap::new;
        Supplier<Map<String, List<Double>>> hashMapStringDoubleSupplier = HashMap::new;
        Supplier<Map<Boolean, List<Double>>> hashMapBooleanDoubleSupplier = HashMap::new;
        Supplier<Map<Double, String>> hashDoubleStringSupplier = HashMap::new;
        Consumer print = (x) -> System.out.println("{" + x + "}");
        Comparator comparator = (Comparator.comparingInt((Integer a) -> a));

        System.out.println("Generate: " + CollectionUtils.generate(random, arrayListIntegerSupplier, 5));
        System.out.println("All match: " + CollectionUtils.allMatch(numbers, isPositive));
        System.out.println("Filter: " + CollectionUtils.filter(numbers, isPositive));
        System.out.println("AnyMatch: " + CollectionUtils.anyMatch(numbers, isPositive));
        System.out.println("NoneMatch: " + CollectionUtils.noneMatch(numbers, isNegative));
        System.out.println("Map: " + CollectionUtils.map(numbers, convertToString, arrayListStringSupplier));
        System.out.println("Max: " + CollectionUtils.max(numbers, comparator));
        System.out.println("Min: " + CollectionUtils.min(numbers, comparator));
        numbers.add(6);
        numbers.add(6);
        CollectionUtils.forEach(numbers, print);
        System.out.println("Distinct: " + CollectionUtils.distinct(numbers, arrayListIntegerSupplier));
        CollectionUtils.forEach(numbers, print);
        System.out.println("Reduce: " + CollectionUtils.reduce(numbers, accumulator));
        System.out.println("Reduce with seed: " + CollectionUtils.reduce(3, numbers, accumulator));
        numbers.add(-2);
        numbers.add(-8);
        numbers.add(-10);
        System.out.println("Partition By: " + CollectionUtils.partitionBy(numbers,
                isPositive, hashMapBooleanIntegerSupplier, arrayListIntegerSupplier));
        System.out.println("Group by: " + CollectionUtils.groupBy(numbers,
                convertToString, hashMapStringIntegerSupplier, arrayListIntegerSupplier));
        System.out.println("toMap: " + CollectionUtils.toMap(numbers,
                convertToDouble, convertToStringSum, accumulatorStr, hashDoubleStringSupplier));
        System.out.println("Partition By and map: " + CollectionUtils.partitionByAndMapElement(numbers,
                isNegative, hashMapBooleanDoubleSupplier, arrayListDoubleSupplier, convertToDouble));
        System.out.println("Group by and map: " + CollectionUtils.groupByAndMapElement(numbers,
                convertToString, hashMapStringDoubleSupplier, arrayListDoubleSupplier, convertToDouble));

    }
}
