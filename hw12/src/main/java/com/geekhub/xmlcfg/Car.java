package com.geekhub.xmlcfg;

import java.util.List;

public class Car {
    Engine engine;
    List<Wheel> wheels;

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public Engine getEngine() {
        return engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public String getInformationAbout() {
        return "Car{" +
                "engine=" + engine.getInformationAbout() +
                ", wheels=" + wheels.toString() +
                '}';
    }
}
