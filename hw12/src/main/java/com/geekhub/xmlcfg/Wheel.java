package com.geekhub.xmlcfg;

public class Wheel {
    private Tyre tyre;

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    public Tyre getTyre() {
        return tyre;
    }

    public String getInformationAbout() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';
    }
}
