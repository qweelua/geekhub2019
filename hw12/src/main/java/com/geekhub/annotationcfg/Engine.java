package com.geekhub.annotationcfg;

import org.springframework.stereotype.Component;

@Component
public class Engine {
    private int capacity;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getInformationAbout() {
        return "Engine{" +
                "capacity=" + capacity +
                '}';
    }
}
