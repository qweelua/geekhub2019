package com.geekhub.annotationcfg;

public abstract class Tyre {
    private int size;
    private String name;

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getInformationAbout();
}
