package com.geekhub.annotationcfg;

import org.springframework.stereotype.Component;

@Component
public class SummerTyre extends Tyre {
    private int size;
    private String name;


    @Override
    public String getInformationAbout() {
        return "It's Summer Tyre";
    }
}
