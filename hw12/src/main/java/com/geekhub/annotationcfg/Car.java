package com.geekhub.annotationcfg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class Car {
    Engine engine;
    List<Wheel> wheels;

    @Autowired
    public void setEngine(com.geekhub.annotationcfg.Engine engine) {
        this.engine = engine;
    }


    @Autowired
    public void setWheels(Wheel wheel, Wheel wheel1, Wheel wheel2, Wheel wheel3) {
        wheels = Arrays.asList(wheel,wheel1,wheel2,wheel3);
    }

    public com.geekhub.annotationcfg.Engine getEngine() {
        return engine;
    }

    public List<com.geekhub.annotationcfg.Wheel> getWheels() {
        return wheels;
    }

    public String getInformationAbout() {
        return "Car{" +
                "engine=" + engine.getInformationAbout() +
                ", wheels=" + wheels.toString() +
                '}';
    }
}
