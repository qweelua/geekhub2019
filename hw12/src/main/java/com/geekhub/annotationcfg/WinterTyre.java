package com.geekhub.annotationcfg;

import org.springframework.stereotype.Component;

@Component
public class WinterTyre extends Tyre {
    private int size;
    private String name;

    @Override
    public String getInformationAbout() {
        return "It's Winter Tyre";
    }
}
