package com.geekhub.annotationcfg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Wheel {
    private Tyre tyre;

    @Autowired
    public void setTyre(@Qualifier("summerTyre") Tyre tyre) {
        this.tyre = tyre;
    }

    public Tyre getTyre() {
        return tyre;
    }


    @Override
    public String toString() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';
    }

    public String getInformationAbout() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';

    }
}
