package com.geekhub.annotationcfg;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContextForAnnotationCfg.xml");
        Wheel wheel = applicationContext.getBean("wheel", Wheel.class);
        System.out.println(wheel.getInformationAbout());
        Car car = applicationContext.getBean("car", Car.class);
        System.out.println(car.getInformationAbout());

    }

}
