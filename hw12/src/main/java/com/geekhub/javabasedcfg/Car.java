package com.geekhub.javabasedcfg;


import java.util.List;

public class Car {
    Engine engine;
    List<Wheel> wheels;

    public Car(Engine engine, List<Wheel> wheels) {
        this.engine = engine;
        this.wheels = wheels;
    }

    public Engine getEngine() {
        return engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public String getInformationAbout() {
        return "Car{" +
                "engine=" + engine.getInformationAbout() +
                ", wheels=" + wheels.toString() +
                '}';
    }
}
