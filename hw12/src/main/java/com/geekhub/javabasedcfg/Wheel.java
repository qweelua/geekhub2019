package com.geekhub.javabasedcfg;

public class Wheel {
    private Tyre tyre;

    public Wheel(Tyre tyre) {
        this.tyre = tyre;
    }

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    public Tyre getTyre() {
        return tyre;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';
    }

    public String getInformationAbout() {
        return "Wheel{" +
                "tyre=" + tyre.getInformationAbout() +
                '}';

    }
}
