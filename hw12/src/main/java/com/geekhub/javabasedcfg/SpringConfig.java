package com.geekhub.javabasedcfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class SpringConfig {
    @Bean
    public Engine engine() {
        return new Engine();
    }

    @Bean
    public Tyre winterTyre() {
        return new WinterTyre();
    }

    @Bean
    public Tyre summerTyre() {
        return new WinterTyre();
    }

    @Bean
    public Wheel wheel() {
        return new Wheel(summerTyre());
    }

    @Bean
    public List<Wheel> putValuesToList() {
        return Arrays.asList(wheel(), wheel(), wheel(), wheel());
    }

    @Bean
    public Car car() {
        return new Car(engine(), putValuesToList());
    }

}
