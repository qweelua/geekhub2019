package com.geekhub.javabasedcfg;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext(
                SpringConfig.class
        );
        Car car = configApplicationContext.getBean("car", Car.class);
        System.out.println(car.getInformationAbout());
    }
}
