<%--
  Created by IntelliJ IDEA.
  com.geekhub.user.User: RYZEN
  Date: 31.01.2020
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<form method="post">
    <textarea name="message"> </textarea>
    Message :
    <br><br>
    <textarea name="message"> </textarea>
    <br><br>
    Rate: <select name=rate>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
</select>
    <br><br>
    <input type="submit" value="Submit"/>
    <a href="<c:url value="/logout"/>">Logout</a>

</form>
<c:forEach var="user" items="${requestScope.users}">
    <ul>
            Name: <c:out value="${user.username}"/><br>

            Message: <c:out value="${user.message}"/><br>

            Rate: <c:out value="${user.rate}"/><br>
    </ul>
</c:forEach>
<br><br><br><br>
<c:forEach var="page" items="${requestScope.pages}">
    <a  href="${pageContext.request.contextPath}/guestbook?page=<c:out value="${page}"/>"><c:out value="${page}"/></a>
</c:forEach>

</body>
</html>
