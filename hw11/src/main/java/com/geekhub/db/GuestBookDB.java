package com.geekhub.db;

import com.geekhub.user.User;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class GuestBookDB {
    static List<User> users = new CopyOnWriteArrayList<>();
    static int usersOnPage = 5;

    public GuestBookDB(List<User> users) {
        this.users = users;
    }

    public static void add(User user) {
        users.add(user);
    }

    public static List<User> getUsers() {
        CopyOnWriteArrayList<User> sortedByDataUsers = users.stream().sorted(Comparator
                .comparing(User::getLocalDateTime))
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
        Collections.reverse(sortedByDataUsers);
        return sortedByDataUsers;
    }

    public static boolean isUserCorrect(String name, String password) {
        User user1 = new User("Bodya", "123");
        User user2 = new User("Test", "1");
        List<User> registerUsers = Arrays.asList(user1, user2);
        for (User user : registerUsers) {
            if (user.getUsername().equals(name) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public static List<Integer> pagesNumbers() {
        List<Integer> pages = new ArrayList<>();
        for (int i = 1; i <= users.size() / usersOnPage + 1; i++) {
            pages.add(i);
        }
        return pages;
    }

    public static List<User> getListOfUsersOnPage(int page) {
        if (page >= pagesNumbers().size()) {
            return getUsers().subList(page * usersOnPage - 5, users.size());
        } else {
            return getUsers().subList(page * usersOnPage - 5, page * usersOnPage);
        }
    }
}
