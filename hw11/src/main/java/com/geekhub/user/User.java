package com.geekhub.user;

import java.time.LocalDateTime;

public class User {
    String username;
    String password;
    String message;
    int rate;
    LocalDateTime localDateTime = LocalDateTime.now();

    public User(String username, String password, String message, int rate) {
        this.username = username;
        this.password = password;
        this.message = message;
        this.rate = rate;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String message, int rate) {
        this.username = username;
        this.message = message;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "com.geekhub.user.User{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", message='" + message + '\'' +
                ", rate=" + rate +
                ", localDateTime=" + localDateTime +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public int getRate() {
        return rate;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
