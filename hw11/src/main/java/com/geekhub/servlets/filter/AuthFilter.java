package com.geekhub.servlets.filter;


import com.geekhub.db.GuestBookDB;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {


        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = ((HttpServletRequest) req).getSession();

        String username = (String) session.getAttribute("username");
        String password = (String) session.getAttribute("password");
        if (!GuestBookDB.isUserCorrect(username, password)) {
            response.sendRedirect("/");
        } else {
            chain.doFilter(request, response);
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
