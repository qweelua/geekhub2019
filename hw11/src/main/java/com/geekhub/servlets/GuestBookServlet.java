package com.geekhub.servlets;

import com.geekhub.db.GuestBookDB;
import com.geekhub.user.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GuestBookServlet extends HttpServlet {

    private final static String index = "/guestBook.jsp";

    @Override
    public void init() throws ServletException {
        for (int i = 0; i < 23; i++) {
            User user = new User("user" + i, i + "-st", 4);
            GuestBookDB.add(user);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        if (page == null) {
            request.setAttribute("users", GuestBookDB.getListOfUsersOnPage(1));
        } else {
            request.setAttribute("users", GuestBookDB.getListOfUsersOnPage(Integer.parseInt(page)));
        }
        request.setAttribute("pages", GuestBookDB.pagesNumbers());
        request.getRequestDispatcher(index).forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String) request.getSession().getAttribute("username");
        String password = (String) request.getSession().getAttribute("password");
        String message = request.getParameter("message");
        String rate = request.getParameter("rate");
        User user = new User(username, password, message, Integer.parseInt(rate));
        GuestBookDB.add(user);
        response.sendRedirect("/guestbook");

    }

}
