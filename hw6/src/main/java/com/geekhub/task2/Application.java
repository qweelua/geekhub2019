package com.geekhub.task2;

import java.io.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Application {
    public static void main(String[] args) throws IOException {
        String folderPath = "hw6\\src\\main\\resources\\testfolder";
        File folder = new File(folderPath);
        String audioZipPath = "hw6\\src\\main\\resources\\audio.zip";
        String videoZipPath = "hw6\\src\\main\\resources\\video.zip";
        String imageZipPath = "hw6\\src\\main\\resources\\image.zip";
        FileOutputStream fileOutputStreamForAudio = new FileOutputStream(new File(audioZipPath));
        FileOutputStream fileOutputStreamForVideo = new FileOutputStream(new File(videoZipPath));
        FileOutputStream fileOutputStreamForImage = new FileOutputStream(new File(imageZipPath));

        archiveFilesFromDirectory(fileOutputStreamForAudio,
                fileOutputStreamForVideo,
                fileOutputStreamForImage,
                folder);

    }

    private static void archiveFilesFromDirectory(FileOutputStream fileOutputStreamForAudio,
                                                  FileOutputStream fileOutputStreamForVideo,
                                                  FileOutputStream fileOutputStreamForImage,
                                                  File folder) throws IOException {
        File[] folderEntries = folder.listFiles();
        if (folderEntries != null) {
            for (File entry : folderEntries) {
                if (entry.isDirectory()) {
                    archiveFilesFromDirectory(fileOutputStreamForAudio,
                            fileOutputStreamForVideo,
                            fileOutputStreamForImage, entry);
                    continue;
                }
                FileInputStream FileInputStream = new FileInputStream(entry);
                if (checkIsAudio(entry)) {
                    ZipOutputStream zipOutputStreamAudio = new ZipOutputStream(fileOutputStreamForAudio);
                    fileToZip(zipOutputStreamAudio, entry, FileInputStream);
                }
                if (checkIsVideo(entry)) {
                    ZipOutputStream zipOutputStreamVideo = new ZipOutputStream(fileOutputStreamForVideo);
                    fileToZip(zipOutputStreamVideo, entry, FileInputStream);
                }
                if (checkIsImage(entry)) {
                    ZipOutputStream zipOutputStreamImage = new ZipOutputStream(fileOutputStreamForImage);
                    fileToZip(zipOutputStreamImage, entry, FileInputStream);
                }

            }
        }
    }

    private static final Predicate<String> AUDIO_PATTERN = Pattern.compile(".+\\.(mp3|wav|wma)").asPredicate();

    private static boolean checkIsAudio(File file) {
        return AUDIO_PATTERN.test(file.getName().toLowerCase());
    }

    private static final Predicate<String> VIDEO_PATTERN = Pattern.compile(".+\\.(avi|mp4|flv)").asPredicate();

    private static boolean checkIsVideo(File file) {
        return VIDEO_PATTERN.test(file.getName().toLowerCase());
    }

    private static final Predicate<String> IMAGE_PATTERN = Pattern.compile(".+\\.(jpeg|jpg|gif|png)").asPredicate();

    private static boolean checkIsImage(File file) {
        return IMAGE_PATTERN.test(file.getName());
    }

    private static void fileToZip(ZipOutputStream zipOutputStream,
                                  File file,
                                  FileInputStream inputStream) throws IOException {
        zipOutputStream.putNextEntry(new ZipEntry(file.getPath()));
        byte[] buffer = new byte[4048];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            zipOutputStream.write(buffer, 0, length);
        }
        zipOutputStream.closeEntry();
        inputStream.close();
    }
}
