package com.geekhub.task1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IOException {
        File source = new File("hw6\\src\\main\\resources\\in.txt");
        File destination = new File("hw6\\src\\main\\resources\\result.txt");

        String text = getStringFromFile(source);
        List<String> wordsList = replaceStringText(text);
        writeTextToFile(destination, wordsList);
    }

    private static void writeTextToFile(File destination, List<String> wordsList) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(destination))) {
            for (String word : wordsList) {
                bufferedWriter.write(word);
                bufferedWriter.newLine();
            }
        }
    }

    private static String getStringFromFile(File file) {
        try (FileInputStream inFile = new FileInputStream(file)) {
            int bufferSize = 4048;
            byte[] buffer = new byte[4048];
            while (inFile.available() > 0) {
                if (inFile.available() < 64000) bufferSize = inFile.available();
                inFile.read(buffer, 0, bufferSize);
                return new String(buffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static ArrayList<String> replaceStringText(String text) {
        ArrayList<String> wordsList = new ArrayList<>();
        String[] words = text.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[^\\w]", "");
            if (words[i].length() >= 10) {
                words[i] = String.valueOf(new StringBuilder()
                        .append(words[i].charAt(0))
                        .append(words[i].length() - 2)
                        .append(words[i].charAt(words[i].length() - 1)));
                wordsList.add(words[i]);
            }
        }
        return wordsList;
    }
}

