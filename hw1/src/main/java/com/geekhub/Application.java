package com.geekhub;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your phone number: ");
        String number = input.nextLine();
        System.out.println(number);
        Pattern pattern = Pattern.compile("^((38093)||(38063)||(38073)||(38067)||(38096)||(38098)||(38097)||(38050)||" +
                "(38066)||(38095)||(38099))\\d{7}$");
        Matcher matcher = pattern.matcher(number);
        number = findNumber(input, number, pattern, matcher);
        System.out.println("Okey! Your number " + number + " is correct");
        long numberLong = Long.parseLong(number);
        int i = 1;
        int sumNumber = 0;
        do {
            sumNumber = 0;
            while (numberLong != 0) {
                sumNumber += (numberLong % 10);
                numberLong /= 10;
            }
            System.out.println(i + "st round is calculation, sum is:" + sumNumber);
            numberLong = sumNumber;
            i += 1;
        }
        while (sumNumber >= 10);
        switch (sumNumber) {
            case 1:
                System.out.println("Final result is: One");
                break;
            case 2:
                System.out.println("Final result is: Two");
                break;
            case 3:
                System.out.println("Final result is: Three");
                break;
            case 4:
                System.out.println("Final result is: Four");
                break;
            default:
                System.out.println("Final result is: " + sumNumber);
        }

    }

    private static String findNumber(Scanner input, String number, Pattern pattern, Matcher matcher) {
        while (!matcher.find()) {
            System.out.println("Phone number:" + number + " is incorrect!\n"
                    + "Phone number should be like 380 and mobile operator’s prefix (“093”, “067” etc.) " +
                    "and 7 other numbers." +
                    "Please enter correct phone number: "
            );
            number = input.nextLine();
            matcher = pattern.matcher(number);
        }
        return number;
    }
}
