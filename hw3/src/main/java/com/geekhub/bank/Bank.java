package com.geekhub.bank;

import com.geekhub.customers.Customer;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    public List<Customer> list = new ArrayList<Customer>();

    public Bank() {
    }

    public void setCustomer(Customer customer) {
        list.add(customer);
    }

    public long calculateWholePriceGrn() {
        long wholePrice = 0;
        for (Customer customer : list) {
            wholePrice += customer.countGrn;
            if (customer.metal != null) {
                wholePrice += customer.calculateWholePricePreciousMetals();
            }
            if (customer.dolars != null) {
                wholePrice += customer.calculateWholeDollars();
            }
            if (customer.euro != null) {
                wholePrice += customer.calculateWholeEuro();
            }
        }
        return wholePrice;
    }
}

