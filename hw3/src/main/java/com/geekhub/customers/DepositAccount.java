package com.geekhub.customers;

import java.time.LocalDate;

public class DepositAccount {
    long initialPrice;
    String currency;
    int percent;
    LocalDate date;

    public DepositAccount(long initialPrice, String currency, int percent, LocalDate date) {
        this.initialPrice = initialPrice;
        this.currency = currency;
        this.percent = percent;
        this.date = date;
    }

    public long CalculateDeposit() {
        int years = date.getYear();
        int depositYears = LocalDate.now().minusYears(years).getYear();
        long sumDeposit = initialPrice + initialPrice * percent / 100 * depositYears;
        return sumDeposit;
    }
}
