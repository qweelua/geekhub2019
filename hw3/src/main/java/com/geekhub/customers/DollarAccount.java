package com.geekhub.customers;

public class DollarAccount {
    long amountMoney;

    public DollarAccount(long amountMoney) {
        this.amountMoney = amountMoney;
    }

    public long converterToGrn() {
        long amountInGrn = amountMoney * 25;
        return amountInGrn;
    }

    public void setAmountMoney(int amountMoney) {
        this.amountMoney = amountMoney;
    }

    public long getAmountMoney() {
        return amountMoney;
    }
}
