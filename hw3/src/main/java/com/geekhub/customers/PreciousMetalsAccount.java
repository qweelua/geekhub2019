package com.geekhub.customers;

public class PreciousMetalsAccount {
    String TypeOfMetal;
    int massInKG;

    public PreciousMetalsAccount(String typeOfMetall, int massInKG) {
        this.TypeOfMetal = typeOfMetall;
        this.massInKG = massInKG;
    }

    public long convertmetall() {
        if (TypeOfMetal == "Gold") {
            long goldToGrn = 1378 * massInKG;
            return goldToGrn;
        }
        if (TypeOfMetal == "Silver") {
            long silverToGrn = 13 * massInKG;
            return silverToGrn;
        } else return 0;
    }

    public String getTypeOfMetal() {
        return TypeOfMetal;
    }

    public int getMassInKG() {
        return massInKG;
    }

    public void setTypeOfMetal(String typeOfMetall) {
        this.TypeOfMetal = typeOfMetall;
    }

    public void setMassInKG(int massInKG) {
        this.massInKG = massInKG;
    }
}

