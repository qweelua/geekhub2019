package com.geekhub.customers;

import com.geekhub.customers.Customer;

public class LegalCustomer extends Customer {
    public int taxPercent;

    public LegalCustomer(String name, String identificationCode, String cardNumber, long countGrn, int taxPercent) {
        super(name, identificationCode, cardNumber, countGrn);
        this.taxPercent = taxPercent;
    }

    public long calculateTax() {
        long tax = countGrn * taxPercent / 100;
        return tax;
    }

    public void setTaxPercent(int taxPercent) {
        this.taxPercent = taxPercent;
    }

    public int getTaxPercent() {
        return taxPercent;
    }
}
