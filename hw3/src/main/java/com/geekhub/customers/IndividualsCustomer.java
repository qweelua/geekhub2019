package com.geekhub.customers;

public class IndividualsCustomer extends Customer {
    public int creditRating;

    public IndividualsCustomer(String name, String identificationCode, String cardNumber, long countGrn, int creditRating) {
        super(name, identificationCode, cardNumber, countGrn);
        this.creditRating = creditRating;
    }

    public int getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(int creditRating) {
        this.creditRating = creditRating;
    }
}
