package com.geekhub.customers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Customer {
    public String name;
    public String identificationCode;
    public String cardNumber;
    public long countGrn;
    public List<DepositAccount> deposit = new ArrayList<>();
    public List<DollarAccount> dolars = new ArrayList<>();
    public List<EuroAccount> euro = new ArrayList<>();
    public List<PreciousMetalsAccount> metal = new ArrayList<>();

    public Customer(String name, String identificationCode, String cardNumber, long countGrn) {
        this.name = name;
        this.identificationCode = identificationCode;
        this.cardNumber = cardNumber;
        this.countGrn = countGrn;
    }

    public long calculateWholeDollars() {
        long wholeSum = 0;
        for (DollarAccount dolar : dolars) {
            wholeSum = wholeSum + dolar.converterToGrn();
        }
        return wholeSum;
    }

    public long calculateWholeEuro() {
        long wholeSum = 0;
        for (EuroAccount euros : euro) {
            wholeSum = wholeSum + euros.converterToGrn();
        }
        return wholeSum;
    }

    public long calculateWholePricePreciousMetals() {
        long wholeSum = 0;
        for (PreciousMetalsAccount metals : metal) {
            wholeSum += metals.convertmetall();
        }
        return wholeSum;
    }

    public long calculateWholeDeposit() {
        long wholeSum = 0;
        for (DepositAccount deposits : deposit) {
            wholeSum += deposits.CalculateDeposit();
        }
        return wholeSum;
    }


    public List<DepositAccount> getDeposit() {
        return deposit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return countGrn == customer.countGrn &&
                name.equals(customer.name) &&
                identificationCode.equals(customer.identificationCode) &&
                cardNumber.equals(customer.cardNumber) &&
                deposit.equals(customer.deposit) &&
                dolars.equals(customer.dolars) &&
                metal.equals(customer.metal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, identificationCode, cardNumber, countGrn, deposit, dolars, metal);
    }

    public long getCountGrn() {
        return countGrn;
    }

    public void setDeposit(long initialPrice, String currency, int percent, LocalDate date) {
        deposit.add(new DepositAccount(initialPrice, currency, percent, date));
    }

    public void setDolars(long amountMoney) {
        dolars.add(new DollarAccount(amountMoney));

    }

    public void setEuro(long amountMoney) {
        euro.add(new EuroAccount(amountMoney));
    }

    public void setPreciousMetals(String typeOfMetall, int massInKG) {
        metal.add(new PreciousMetalsAccount(typeOfMetall, massInKG));
    }

    public String getTypeMetal() {
        String allTypeMetal = " ";
        for (PreciousMetalsAccount metals : metal) {
            allTypeMetal = metals.TypeOfMetal + " ";
        }
        return allTypeMetal;
    }

    public int getMetalKG() {
        int allKG = 0;
        for (PreciousMetalsAccount metals : metal) {
            allKG += metals.massInKG;
        }
        return allKG;
    }

    public long getAmountDollars() {
        long wholeDollar = 0;
        for (DollarAccount dolar : dolars) {
            wholeDollar += dolar.amountMoney;
        }
        return wholeDollar;
    }
}


