package com.geekhub.customers;

public class EuroAccount {
    long amountMoney;

    public EuroAccount(long amountMoney) {
        this.amountMoney = amountMoney;
    }

    public long converterToGrn() {
        long amountInGrn = amountMoney * 28;
        return amountInGrn;
    }

    public long getAmountMoney() {
        return amountMoney;
    }
}
