package com.geekhub;

import com.geekhub.bank.Bank;
import com.geekhub.customers.IndividualsCustomer;
import com.geekhub.customers.LegalCustomer;

import java.time.LocalDate;
import java.time.Month;

public class Application {

    public static void main(String[] args) {

        IndividualsCustomer customer1 = new IndividualsCustomer("Bodya", "123123132", "123123123", 100, 5);
        LegalCustomer customer2 = new LegalCustomer("Vasya", "12777732", "9999999999", 20000, 10);
        LocalDate startDay = LocalDate.of(2014, Month.MARCH, 21);
        customer1.setDeposit(1000, "Dolars", 10, startDay);
        printDeposit(customer1);
        customer1.setDolars(2000);
        printDollar(customer1);
        customer2.setPreciousMetals("Gold", 10);
        printMetals(customer2);
        printTax(customer2);
        Bank bank = new Bank();
        bank.setCustomer(customer1);
        bank.setCustomer(customer2);
        printWholePrice(bank);
    }

    private static void printWholePrice(Bank bank) {
        System.out.println("Whole price in bank is " + bank.calculateWholePriceGrn() + " GRN");
    }

    private static void printTax(LegalCustomer customer2) {
        System.out.println(customer2.name + " have " + customer2.taxPercent + " % of tax. His tax in GRN will be " + customer2.calculateTax());
    }

    private static void printMetals(LegalCustomer customer2) {
        System.out.println(customer2.name + " have " + customer2.getMetalKG() + " KG of "
                + customer2.getTypeMetal() + " = " + customer2.calculateWholePricePreciousMetals() + " GRN");
    }

    private static void printDollar(IndividualsCustomer customer1) {
        System.out.println(customer1.name + " have "
                + customer1.getAmountDollars() + " USD = " + customer1.calculateWholeDollars() + " GRN");
    }

    private static void printDeposit(IndividualsCustomer customer1) {
        System.out.println(customer1.name + " have deposit. His avrg deposit will be " + customer1.calculateWholeDeposit());
    }
}