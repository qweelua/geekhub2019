package com.geekhub.movieblog.feedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class FeedBackService {
    private final FeedBackRepository feedBackRepository;

    @Autowired
    public FeedBackService(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }

    public FeedBack createFeedBack(int movieId, String username, String comment, int rate) {
        FeedBack feedBack = new FeedBack();
        feedBack.setMovieId(movieId);
        feedBack.setUsername(username);
        feedBack.setComment(comment);
        feedBack.setRate(rate);
        feedBack.setDateTime(LocalDateTime.now());
        return feedBackRepository.saveFeedBack(feedBack);
    }

    public List<FeedBack> getFeedBackByMovieIdSortedByDate(int movieId) {
        return feedBackRepository.getFeedBackByMovieIdSortedByDate(movieId);
    }

    public float getAvgRateForMovieById(int movieId) {
        return feedBackRepository.getAvgRateForMovieById(movieId);
    }

    public int getCountOfFeedBacks(int movieId) {
        return feedBackRepository.getCountOfFeedBacks(movieId);
    }
}
