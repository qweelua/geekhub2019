package com.geekhub.movieblog.feedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class FeedBackRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public FeedBackRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public FeedBack saveFeedBack(FeedBack feedBack) {
        String sql = "INSERT INTO feedback(movie_id, username, comment, rate, date) VALUES (:movieId, :username, :comment, :rate, :date) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movieId", feedBack.getMovieId());
        params.addValue("username", feedBack.getUsername());
        params.addValue("comment", feedBack.getComment());
        params.addValue("rate", feedBack.getRate());
        params.addValue("date", feedBack.getDateTime().toString());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int id = keyHolder.getKey().intValue();

        feedBack.setId(id);

        return feedBack;
    }

    public List<FeedBack> getFeedBackByMovieIdSortedByDate(int movieId) {
        String sql = "SELECT id, username, comment, rate, date FROM feedback WHERE movie_id = :movieId ORDER BY date desc";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movieId", movieId);

        return jdbcTemplate.query(sql, params, (rs, rowNum) -> {

            FeedBack feedBack = new FeedBack();
            feedBack.setId(rs.getInt("id"));
            feedBack.setUsername(rs.getString("username"));
            feedBack.setComment(rs.getString("comment"));
            feedBack.setRate(rs.getInt("rate"));
            feedBack.setDateTime(LocalDateTime.parse(rs.getString("date")));
            return feedBack;

        });
    }

    public float getAvgRateForMovieById(int movieId) {
        String sql = "SELECT AVG(rate) FROM feedback WHERE movie_id = :movieId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movieId", movieId);

        Float avgRate = jdbcTemplate.queryForObject(sql, params, Float.class);

        if (avgRate != null)
            return avgRate;
        return 0;
    }

    public int getCountOfFeedBacks(int movieId) {
        String sql = "SELECT COUNT(rate) FROM feedback WHERE movie_id = :movieId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movieId", movieId);

        return jdbcTemplate.queryForObject(sql, params, Integer.class);
    }
}
