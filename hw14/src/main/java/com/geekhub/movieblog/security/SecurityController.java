package com.geekhub.movieblog.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SecurityController {
    private final UserRepository userRepository;

    @Autowired
    public SecurityController(UserRepository userRepository) {
        this.userRepository = userRepository;
        //userRepository.createAdmin();
    }

    @GetMapping("/registration")
    public String registration() {
        return "/registration";
    }


    @PostMapping("/registration")
    public String saveUser(@RequestParam String username, @RequestParam String password) {
        userRepository.saveUser(username, password);
        return "redirect:/movies";
    }

    @GetMapping("login")
    public String login() {
        return "/login";
    }
}
