package com.geekhub.movieblog.security;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final HikariDataSource hikariDataSource;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityConfiguration(HikariDataSource hikariDataSource, PasswordEncoder passwordEncoder) {
        this.hikariDataSource = hikariDataSource;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .jdbcAuthentication()
                .dataSource(hikariDataSource)
                .usersByUsernameQuery("SELECT username, password, 'TRUE' enabled FROM \"user\" WHERE username = ?")
                .authoritiesByUsernameQuery("SELECT username, user_role authority FROM \"user\" WHERE username = ?")
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/authors/create").hasRole("ADMIN")
                .antMatchers("/movies/create").hasRole("ADMIN")
                .antMatchers("/authors/{movieId}/show").hasAnyRole("USER", "ADMIN")
                .antMatchers("/movies/{movieId}/show").hasAnyRole("USER", "ADMIN")
                .and()
                .csrf().disable()
                .formLogin().loginPage("/login").permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
                .and()
                .rememberMe();
    }

}
