package com.geekhub.movieblog.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    private final PasswordEncoder passwordEncoder;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }

    public void createAdmin() {
        String sql = "INSERT INTO \"user\" (username, password, user_role) VALUES (:username, :password, :user_role)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", "admin");
        params.addValue("password", passwordEncoder.encode("admin"));
        params.addValue("user_role", "ROLE_ADMIN");

        jdbcTemplate.update(sql, params);
    }

    public User saveUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));

        String sql = "INSERT INTO \"user\" (username, password, user_role) VALUES (:username, :password, :user_role)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", user.getUsername());
        params.addValue("password", user.getPassword());
        params.addValue("user_role", "ROLE_USER");

        jdbcTemplate.update(sql, params);
        return user;
    }
}
