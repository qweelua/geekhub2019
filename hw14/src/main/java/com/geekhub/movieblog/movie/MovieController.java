package com.geekhub.movieblog.movie;

import com.geekhub.movieblog.author.*;
import com.geekhub.movieblog.authormovie.AuthorMovieRelation;
import com.geekhub.movieblog.authormovie.AuthorMovieRelationService;
import com.geekhub.movieblog.feedback.FeedBack;
import com.geekhub.movieblog.feedback.FeedBackService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/movies")
public class MovieController {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final AuthorMovieRelationService authorMovieRelationService;
    private final FeedBackService feedBackService;
    private final MovieToFileConverter movieToFileConverter;

    private final MovieDtoConverter movieDtoConverter;
    private final AuthorDtoConverter authorDtoConverter;

    @Autowired
    public MovieController(MovieService movieService,
                           AuthorService authorService,
                           AuthorMovieRelationService authorMovieRelationService,
                           FeedBackService feedBackService,
                           MovieToFileConverter movieToFileConverter, MovieDtoConverter movieDtoConverter,
                           AuthorDtoConverter authorDtoConverter) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.feedBackService = feedBackService;
        this.movieToFileConverter = movieToFileConverter;
        this.movieDtoConverter = movieDtoConverter;
        this.authorDtoConverter = authorDtoConverter;
    }

    @GetMapping
    public String getIndexPage(ModelMap model, HttpServletResponse response) throws DocumentException, IOException {
        List<Movie> allMovies = movieService.getAllMovies();
        List<MovieWithCountOfCommentsAndAvgRateDto> allMovieDto = new ArrayList<>();
        for (Movie movie : allMovies) {
            MovieWithCountOfCommentsAndAvgRateDto convertedMovie = movieDtoConverter.convert(movie,
                    feedBackService.getCountOfFeedBacks(movie.getId()),
                    feedBackService.getAvgRateForMovieById(movie.getId()));
            allMovieDto.add(convertedMovie);
        }
        model.addAttribute("movies", allMovieDto);
        return "movie/index";
    }

    @GetMapping("/pdf")
    public void downloadPDFFile(HttpServletResponse response) throws DocumentException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = movieToFileConverter.createPdfDocument();
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=Movies.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }

    @GetMapping("/doc")
    public void downloadDocFile(HttpServletResponse response) throws IOException {
        XWPFDocument document = movieToFileConverter.createDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Movies.docx");
        OutputStream os = response.getOutputStream();
        document.write(os);
        os.flush();
        os.close();
    }

    @GetMapping("/excel")
    public void downloadExcelFile(HttpServletResponse response) throws IOException {
        Workbook excelDocument = movieToFileConverter.createExcelDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Movies.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }


    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genres", GenreDto.values());
        model.addAttribute("languages", LanguageDto.values());
        List<Author> allAuthors = authorService.getAllAuthors();
        List<AuthorDto> allAuthorsDto = new ArrayList<>();
        for (Author author : allAuthors) {
            AuthorDto convertedAuthor = authorDtoConverter.convert(author);
            allAuthorsDto.add(convertedAuthor);
        }
        model.addAttribute("authors", allAuthorsDto);

        return "movie/create";
    }

    @PostMapping("/create")
    public String createMovie(@RequestParam String name,
                              @RequestParam int year,
                              @RequestParam GenreDto genre,
                              @RequestParam LanguageDto language,
                              @RequestParam List<Integer> authorIds) {

        Movie movie = movieService.createMovie(name, year, Genre.valueOf(genre.name()),
                Language.valueOf(language.name()), authorIds);
        return "redirect:/movies/" + movie.getId() + "/show";
    }

    @GetMapping("/{movieId}/show")
    public String getShowMoviePage(@PathVariable int movieId, ModelMap model) {
        Movie movie = movieService.getMovieById(movieId);
        MovieDto convertedMovie = movieDtoConverter.convert(movie);
        List<AuthorDto> authors = new ArrayList<>();
        List<AuthorMovieRelation> authorMovieRelationByMovieId =
                authorMovieRelationService.getAuthorMovieRelationByMovieId(movieId);
        for (AuthorMovieRelation authorMovieRelation : authorMovieRelationByMovieId) {
            Author author = authorService.getAuthorById(authorMovieRelation.getAuthorId());
            AuthorDto convertedAuthor = authorDtoConverter.convert(author);
            authors.add(convertedAuthor);
        }
        List<FeedBack> feedBacksByMovieId = feedBackService.getFeedBackByMovieIdSortedByDate(movieId);
        model.addAttribute("feedBacks", feedBacksByMovieId);
        model.addAttribute("movie", convertedMovie);
        model.addAttribute("authors", authors);
        return "movie/show";
    }

    @PostMapping("/{movieId}/show")
    public String createFeedBack(@PathVariable int movieId,
                                 @RequestParam String username,
                                 @RequestParam String comment,
                                 @RequestParam int rate) {

        feedBackService.createFeedBack(movieId, username, comment, rate);
        return "redirect:/movies/" + movieId + "/show";
    }
}