package com.geekhub.movieblog.movie;

public enum GenreDto {
    COMEDY, ACTION, PARODY
}
