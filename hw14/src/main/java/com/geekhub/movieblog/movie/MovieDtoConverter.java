package com.geekhub.movieblog.movie;

import org.springframework.stereotype.Component;

@Component
public class MovieDtoConverter {

    public MovieDto convert(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setName(movie.getName());
        movieDto.setYear(movie.getYear());

        GenreDto convertedGenre = GenreDto.valueOf(movie.getGenre().name());
        movieDto.setGenre(convertedGenre);

        LanguageDto convertedLanguage = LanguageDto.valueOf(movie.getLanguage().name());
        movieDto.setLanguage(convertedLanguage);

        return movieDto;
    }

    public MovieWithCountOfCommentsAndAvgRateDto convert(Movie movie, int countOfComments, float avgRate) {
        MovieWithCountOfCommentsAndAvgRateDto movieWithCountOfCommentsAndAvgRateDto = new MovieWithCountOfCommentsAndAvgRateDto();
        movieWithCountOfCommentsAndAvgRateDto.setId(movie.getId());
        movieWithCountOfCommentsAndAvgRateDto.setName(movie.getName());
        movieWithCountOfCommentsAndAvgRateDto.setYear(movie.getYear());

        GenreDto convertedGenre = GenreDto.valueOf(movie.getGenre().name());
        movieWithCountOfCommentsAndAvgRateDto.setGenre(convertedGenre);

        LanguageDto convertedLanguage = LanguageDto.valueOf(movie.getLanguage().name());
        movieWithCountOfCommentsAndAvgRateDto.setLanguage(convertedLanguage);

        movieWithCountOfCommentsAndAvgRateDto.setCountOfComments(countOfComments);
        movieWithCountOfCommentsAndAvgRateDto.setAvgRate(avgRate);

        return movieWithCountOfCommentsAndAvgRateDto;
    }
}
