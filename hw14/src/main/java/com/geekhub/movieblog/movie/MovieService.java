package com.geekhub.movieblog.movie;

import com.geekhub.movieblog.authormovie.AuthorMovieRelation;
import com.geekhub.movieblog.authormovie.AuthorMovieRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class MovieService {

    private final MovieRepository movieRepository;
    private final AuthorMovieRelationRepository authorMovieRelationRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository, AuthorMovieRelationRepository authorMovieRelationRepository) {
        this.movieRepository = movieRepository;
        this.authorMovieRelationRepository = authorMovieRelationRepository;
    }

    public List<Movie> getAllMovies() {
        return movieRepository.getAllMovies();
    }

    public Movie createMovie(String name, int year, Genre genre, Language language, List<Integer> authorIds) {
        Movie movie = new Movie();
        movie.setName(name);
        movie.setYear(year);
        movie.setGenre(genre);
        movie.setLanguage(language);
        movieRepository.saveMovie(movie);
        for (Integer authorId : authorIds) {
            AuthorMovieRelation authorMovieRelation = new AuthorMovieRelation();
            authorMovieRelation.setAuthorId(authorId);
            authorMovieRelation.setMovieId(movie.getId());
            authorMovieRelationRepository.saveAuthorMovieRelation(authorMovieRelation);
        }

        return movie;
    }

    public Movie getMovieById(int movieId) {
        return movieRepository.getMovieById(movieId);
    }

    public List<Movie> getMoviesByIds(List<Integer> movieIds) {
        if (movieIds.isEmpty()) {
            return Collections.emptyList();
        }
        return movieRepository.getMoviesByIds(movieIds);
    }
}
