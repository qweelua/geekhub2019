package com.geekhub.movieblog.movie;

public class MovieWithCountOfCommentsAndAvgRateDto extends MovieDto {
    private int countOfComments;
    private float avgRate;

    public int getCountOfComments() {
        return countOfComments;
    }

    public void setCountOfComments(int countOfComments) {
        this.countOfComments = countOfComments;
    }

    public float getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(float avgRate) {
        this.avgRate = avgRate;
    }
}
