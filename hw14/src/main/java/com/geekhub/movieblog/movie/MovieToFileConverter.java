package com.geekhub.movieblog.movie;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.itextpdf.text.pdf.PdfPTable;

import java.io.ByteArrayOutputStream;
import java.util.List;

@Component
public class MovieToFileConverter {

    private final MovieService movieService;

    @Autowired
    public MovieToFileConverter(MovieService movieService) {
        this.movieService = movieService;
    }

    public ByteArrayOutputStream createPdfDocument() throws DocumentException {
        PdfPTable table = new PdfPTable(4);
        table.addCell("NAME");
        table.addCell("YEAR");
        table.addCell("GENRE");
        table.addCell("LANGUAGE");
        List<Movie> allMovies = movieService.getAllMovies();
        for (Movie movie : allMovies) {
            table.addCell(movie.getName());
            table.addCell(String.valueOf(movie.getYear()));
            table.addCell(movie.getGenre().name());
            table.addCell(movie.getLanguage().name());
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(table);
        document.close();
        return byteArrayOutputStream;
    }

    public Workbook createExcelDocument() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Movies");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("NAME");
        row.createCell(1).setCellValue("YEAR");
        row.createCell(2).setCellValue("GENRE");
        row.createCell(3).setCellValue("LANGUAGE");
        int rowIter = 1;
        List<Movie> allMovies = movieService.getAllMovies();
        for (Movie movie : allMovies) {
            Row tempRaw = sheet.createRow(rowIter);
            rowIter++;
            tempRaw.createCell(0).setCellValue(movie.getName());
            tempRaw.createCell(1).setCellValue(movie.getYear());
            tempRaw.createCell(2).setCellValue(movie.getGenre().name());
            tempRaw.createCell(3).setCellValue(movie.getLanguage().name());
        }
        return workbook;
    }

    public XWPFDocument createDocument() {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable();
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("NAME");
        tableRowOne.addNewTableCell().setText("YEAR");
        tableRowOne.addNewTableCell().setText("GENRE");
        tableRowOne.addNewTableCell().setText("LANGUAGE");
        List<Movie> allMovies = movieService.getAllMovies();
        for (Movie movie : allMovies) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(movie.getName());
            tableRowTwo.getCell(1).setText(String.valueOf(movie.getYear()));
            tableRowTwo.getCell(2).setText(movie.getGenre().name());
            tableRowTwo.getCell(3).setText(movie.getLanguage().name());
        }
        return document;
    }
}
