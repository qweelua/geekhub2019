package com.geekhub.movieblog.authormovie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorMovieRelationService {

    private final AuthorMovieRelationRepository authorMovieRelationRepository;

    @Autowired
    public AuthorMovieRelationService(AuthorMovieRelationRepository authorMovieRelationRepository) {
        this.authorMovieRelationRepository = authorMovieRelationRepository;
    }

    public List<AuthorMovieRelation> getAuthorMovieRelationByMovieId(int movieId) {
        return authorMovieRelationRepository.getAuthorMovieRelationByMovieId(movieId);
    }

    public List<AuthorMovieRelation> getAuthorMovieRelationByAuthorId(int authorId) {
        return authorMovieRelationRepository.getAuthorMovieRelationByAuthorId(authorId);
    }
}
