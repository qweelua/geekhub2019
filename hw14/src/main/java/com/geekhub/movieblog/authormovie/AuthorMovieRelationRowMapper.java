package com.geekhub.movieblog.authormovie;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AuthorMovieRelationRowMapper implements RowMapper<AuthorMovieRelation> {

    @Override
    public AuthorMovieRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
        AuthorMovieRelation authorMovieRelation = new AuthorMovieRelation();
        authorMovieRelation.setAuthorId(rs.getInt("author_id"));
        authorMovieRelation.setMovieId(rs.getInt("movie_id"));

        return authorMovieRelation;
    }
}
