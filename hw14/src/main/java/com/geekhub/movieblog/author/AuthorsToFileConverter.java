package com.geekhub.movieblog.author;

import com.geekhub.movieblog.movie.Movie;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.List;

@Component
public class AuthorsToFileConverter {
    private final AuthorService authorService;

    @Autowired
    public AuthorsToFileConverter(AuthorService authorService) {
        this.authorService = authorService;
    }

    public ByteArrayOutputStream createPdfDocument() throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.addCell("NAME");
        table.addCell("GENDER");
        List<Author> allAuthors = authorService.getAllAuthors();
        for (Author author : allAuthors) {
            table.addCell(author.getName());
            table.addCell(author.getGender().name());
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(table);
        document.close();
        return byteArrayOutputStream;
    }

    public Workbook createExcelDocument() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Authors");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("NAME");
        row.createCell(1).setCellValue("GENDER");
        int rowIter = 1;
        List<Author> allAuthors = authorService.getAllAuthors();
        for (Author author : allAuthors) {
            Row tempRaw = sheet.createRow(rowIter);
            rowIter++;
            tempRaw.createCell(0).setCellValue(author.getName());
            tempRaw.createCell(1).setCellValue(author.getGender().name());
        }
        return workbook;
    }

    public XWPFDocument createDocument() {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable();
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("NAME");
        tableRowOne.addNewTableCell().setText("GENDER");
        List<Author> allAuthors = authorService.getAllAuthors();
        for (Author author : allAuthors) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(author.getName());
            tableRowTwo.getCell(1).setText(author.getGender().name());
        }
        return document;
    }
}
