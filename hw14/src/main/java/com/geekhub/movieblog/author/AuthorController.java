package com.geekhub.movieblog.author;

import com.geekhub.movieblog.authormovie.AuthorMovieRelation;
import com.geekhub.movieblog.authormovie.AuthorMovieRelationService;
import com.geekhub.movieblog.movie.Movie;
import com.geekhub.movieblog.movie.MovieDto;
import com.geekhub.movieblog.movie.MovieDtoConverter;
import com.geekhub.movieblog.movie.MovieService;
import com.itextpdf.text.DocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/authors")
public class AuthorController {

    private final AuthorService authorService;
    private final AuthorMovieRelationService authorMovieRelationService;
    private final MovieService movieService;
    private final AuthorsToFileConverter authorsToFileConverter;

    private final AuthorDtoConverter authorDtoConverter;
    private final MovieDtoConverter movieDtoConverter;

    @Autowired
    public AuthorController(AuthorService authorService,
                            AuthorMovieRelationService authorMovieRelationService,
                            MovieService movieService,
                            AuthorsToFileConverter authorsToFileConverter, AuthorDtoConverter authorDtoConverter,
                            MovieDtoConverter movieDtoConverter) {
        this.authorService = authorService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.movieService = movieService;
        this.authorsToFileConverter = authorsToFileConverter;
        this.authorDtoConverter = authorDtoConverter;
        this.movieDtoConverter = movieDtoConverter;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<Author> allAuthors = authorService.getAllAuthors();
        Map<Integer, Integer> movieCountByAuthorId = authorService.getMovieCountByAuthorId();
        List<AuthorWithMovieCountDto> authorWithMovieCountDtos = new ArrayList<>();
        if (!allAuthors.isEmpty()) {
            for (Author author : allAuthors) {
                Integer countOfMovies = movieCountByAuthorId.get(author.getId());
                if (countOfMovies == null) countOfMovies = 0;
                AuthorWithMovieCountDto convertedAuthorWithCountDto = authorDtoConverter.convert(author, countOfMovies);
                authorWithMovieCountDtos.add(convertedAuthorWithCountDto);
            }
        }
        model.addAttribute("authors", authorWithMovieCountDtos);

        return "author/index";
    }

    @GetMapping("/pdf")
    public void downloadPDFFile(HttpServletResponse response) throws DocumentException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = authorsToFileConverter.createPdfDocument();
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=Authors.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }

    @GetMapping("/doc")
    public void downloadDocFile(HttpServletResponse response) throws IOException {
        XWPFDocument document = authorsToFileConverter.createDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Authors.docx");
        OutputStream os = response.getOutputStream();
        document.write(os);
        os.flush();
        os.close();
    }

    @GetMapping("/excel")
    public void downloadExcelFile(HttpServletResponse response) throws IOException {
        Workbook excelDocument = authorsToFileConverter.createExcelDocument();
        response.setHeader("Content-Disposition", "attachment; filename=Authors.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }

    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genders", GenderDto.values());
        return "author/create";
    }

    @PostMapping("/create")
    public String createAuthor(@RequestParam String name,
                               @RequestParam GenderDto gender) {
        Author author = authorService.createAuthor(name, Gender.valueOf(gender.name()));
        return "redirect:/authors/" + author.getId() + "/show";
    }

    @GetMapping("/{authorId}/show")
    public String getShowAuthorPage(@PathVariable int authorId, ModelMap model) {
        Author author = authorService.getAuthorById(authorId);
        AuthorDto convertedAuthor = authorDtoConverter.convert(author);
        List<MovieDto> movies = new ArrayList<>();
        List<AuthorMovieRelation> authorMovieRelationByAuthorId =
                authorMovieRelationService.getAuthorMovieRelationByAuthorId(authorId);
        for (AuthorMovieRelation authorMovieRelation : authorMovieRelationByAuthorId) {
            Movie movie = movieService.getMovieById(authorMovieRelation.getMovieId());
            MovieDto convertedMovie = movieDtoConverter.convert(movie);
            movies.add(convertedMovie);
        }
        model.addAttribute("movies", movies);
        model.addAttribute("author", convertedAuthor);
        return "author/show";
    }
}
