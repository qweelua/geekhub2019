package com.geekhub.movieblog.author;

import org.springframework.stereotype.Component;

/**
 * This component is used to convert entity objects to dto objects
 */
@Component
public class AuthorDtoConverter {

    public AuthorDto convert(Author author) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(author.getId());
        authorDto.setName(author.getName());

        GenderDto convertedGender = GenderDto.valueOf(author.getGender().name());
        authorDto.setGender(convertedGender);

        return authorDto;
    }

    public AuthorWithMovieCountDto convert(Author author, int movieCount) {
        AuthorWithMovieCountDto authorWithMovieCountDto = new AuthorWithMovieCountDto();
        authorWithMovieCountDto.setId(author.getId());
        authorWithMovieCountDto.setName(author.getName());

        GenderDto convertedGender = GenderDto.valueOf(author.getGender().name());
        authorWithMovieCountDto.setGender(convertedGender);

        authorWithMovieCountDto.setMovieCount(movieCount);

        return authorWithMovieCountDto;
    }
}
