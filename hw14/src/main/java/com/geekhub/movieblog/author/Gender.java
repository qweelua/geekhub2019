package com.geekhub.movieblog.author;

public enum Gender {
    MALE, FEMALE
}
