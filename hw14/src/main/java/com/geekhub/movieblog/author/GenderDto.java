package com.geekhub.movieblog.author;

public enum GenderDto {
    MALE, FEMALE
}
