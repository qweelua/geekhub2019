package com.geekhub.movieblog.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuthorRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final AuthorRowMapper authorRowMapper;

    @Autowired
    public AuthorRepository(NamedParameterJdbcTemplate jdbcTemplate, AuthorRowMapper authorRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.authorRowMapper = authorRowMapper;
    }

    public Author saveAuthor(Author author) {
        String sql = "INSERT INTO author (name, gender) VALUES (:name, :gender) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", author.getName());
        params.addValue("gender", author.getGender().name());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        author.setId(generatedId);

        return author;
    }

    public List<Author> getAllAuthors() {
        String sql = "SELECT id, name, gender FROM author ORDER BY id ASC";
        return jdbcTemplate.query(sql, authorRowMapper);
    }

    public List<Author> getAuthorsByIds(List<Integer> authorIds) {
        String sql = "SELECT id, name, gender FROM author WHERE id IN (:authorsIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("authorsIds", authorIds);

        return jdbcTemplate.query(sql, params, authorRowMapper);
    }

    public Author getAuthorById(int authorId) {
        String sql = "SELECT id, name, gender FROM author WHERE id = :authorId";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("authorId", authorId);

        return jdbcTemplate.queryForObject(sql, params, authorRowMapper);
    }
}
