package com.geekhub.movieblog.author;

public class AuthorWithMovieCountDto extends AuthorDto {

    private int movieCount;

    public int getMovieCount() {
        return movieCount;
    }

    public void setMovieCount(int movieCount) {
        this.movieCount = movieCount;
    }
}
