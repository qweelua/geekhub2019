create table feedback
(
    id serial
        constraint feedback_pk
            primary key,
    movie_id int not null,
    username varchar(20) not null,
    comment varchar(70) not null,
    rate int not null,
    date varchar(30) not null
);
