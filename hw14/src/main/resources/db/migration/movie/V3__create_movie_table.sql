create table movie
(
    id serial primary key,
    name varchar not null,
    year int not null,
    genre varchar(20) not null,
    language varchar(20) not null
);