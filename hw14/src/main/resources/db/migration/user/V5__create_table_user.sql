create table "user"
(
    username  varchar not null
        constraint user_pkey
            primary key,
    password  varchar not null,
    user_role varchar not null
);